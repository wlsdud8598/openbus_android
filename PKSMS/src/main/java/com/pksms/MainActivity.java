package com.pksms;

import android.Manifest;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ensurePermissions(Manifest.permission.SEND_SMS);

        findViewById(R.id.send_sms_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SMSUtil.sendSMS(MainActivity.this,"01022587654", "테스트메시지테스트메시지테스트메시지테스트메시지테스트메시지테스트메시지테스트메시지테스트메시지테스트메시지테스트메시지테스트메시지테스트메시지테스트메시지테스트메시지테스트메시지테스트메시지테스트메시지테스트메시지테스트메시지테스트메시지테스트메시지테스트메시지테스트메시지");
            }
        });

        findViewById(R.id.service_start_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ForeGroundService.startForegroundService(MainActivity.this);
            };
        });

    }

}
