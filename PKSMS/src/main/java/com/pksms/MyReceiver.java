package com.pksms;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class MyReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        // an Intent broadcast.
//        throw new UnsupportedOperationException("Not yet implemented");
        if (intent.getAction().equals(Intent.ACTION_BOOT_COMPLETED)) {
            ForeGroundService.startForegroundService(context);
        }
    }

//    private void startForegroundService(Context context) {
//        Intent intent = new Intent(context, ForeGroundService.class);
//        if (Build.VERSION.SDK_INT >= 26) {
//            context.startForegroundService(intent);
//        } else {
//            context.startService(intent);
//        }
//    }
}
