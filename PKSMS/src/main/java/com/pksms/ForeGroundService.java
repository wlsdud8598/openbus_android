package com.pksms;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Build;
import android.os.IBinder;
import android.util.Log;
import android.widget.RemoteViews;

import com.google.firebase.FirebaseApp;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import androidx.annotation.Nullable;
import androidx.core.app.NotificationCompat;

public class ForeGroundService extends Service {
    private final String TAG = ForeGroundService.class.getSimpleName();

    public static void startForegroundService(Context context) {
        Intent intent = new Intent(context, ForeGroundService.class);
        if (Build.VERSION.SDK_INT >= 26) {
            context.startForegroundService(intent);
        } else {
            context.startService(intent);
        }
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        FirebaseApp.initializeApp(this);
        startForegroundService();
    }

    void startForegroundService() {
        RemoteViews remoteViews = new RemoteViews(getPackageName(), R.layout.notification_service);
        Intent rewIntent = new Intent("com.pksms.startsms");
        PendingIntent rewPendingIntent = PendingIntent.getBroadcast(this, 1, rewIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        remoteViews.setOnClickPendingIntent(R.id.sms_service_start, rewPendingIntent);

        rewIntent = new Intent("com.pksms.stopservice");
        rewPendingIntent = PendingIntent.getBroadcast(this, 1, rewIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        remoteViews.setOnClickPendingIntent(R.id.stop_service_btn, rewPendingIntent);

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy.MM.dd HH.mm:ss");
        remoteViews.setTextViewText(R.id.log_time_textview, sdf.format(new Date()));

        IntentFilter filter = new IntentFilter();
        filter.addAction("com.pksms.startsms");
        filter.addAction("com.pksms.stopservice");
        registerReceiver(broadcastReceiver, filter);

        Intent notificationIntent = new Intent(this, MainActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, notificationIntent, 0);
        NotificationCompat.Builder builder;
        if (Build.VERSION.SDK_INT >= 26) {
            String CHANNEL_ID = "snwodeer_service_channel";
            NotificationChannel channel = new NotificationChannel(CHANNEL_ID,
                    "SnowDeer Service Channel",
                    NotificationManager.IMPORTANCE_DEFAULT);

            ((NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE)).createNotificationChannel(channel);

            builder = new NotificationCompat.Builder(this, CHANNEL_ID);
        } else {
            builder = new NotificationCompat.Builder(this);
        }
        builder.setSmallIcon(R.mipmap.ic_launcher)
                .setContent(remoteViews)
                .setContentIntent(pendingIntent);

        startForeground(1, builder.build());

        scheduleSms();
    }

    /**
     * Notification 버튼 클릭
     */
    private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals("com.pksms.startsms")) {
                scheduleSms();
            } else if(intent.getAction().equals("com.pksms.stopservice")) {
                Intent serviceIntent = new Intent(ForeGroundService.this, ForeGroundService.class);
                stopService(serviceIntent);
            }
        }
    };

    private void scheduleSms() {
        try {
            Log.i(TAG, "scheduleSms");
            Timer jobScheduler = new Timer();
//            int period = 60 * 60 * 1000;    // 1 시간
            int period = 10 * 60 * 1000;    // 10분
            Woker woker = new Woker();
            jobScheduler.schedule(woker, 100, period);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    class Woker extends TimerTask {
        @Override
        public void run() {
//            SMSUtil.sendSMS(ForeGroundService.this, "01022587654", "테스트메시지테스트메시지테스트메시지테스트메시지테스트메시지테스트메시지테스트메시지테스트메시지테스트메시지테스트메시지테스트메시지테스트메시지테스트메시지테스트메시지테스트메시지테스트메시지테스트메시지테스트메시지테스트메시지테스트메시지테스트메시지테스트메시지테스트메시지테스트메시지테스트메시지테스트메시지테스트메시지테스트메시지테스트메시지테스트메시지테스트메시지테스트메시지테스트메시지테스트메시지테스트메시지테스트메시지테스트메시지테스트메시지테스트메시지");

            FirebaseDatabase database = FirebaseDatabase.getInstance();
            SimpleDateFormat df = new SimpleDateFormat("MM.dd HH:mm:ss");
            String time = df.format(new Date());

            DatabaseReference myRef = database.getReference("log");
            String key = myRef.push().getKey();    // log의 랜덤 키 생성
            Map<String, Object> map = new HashMap<>();
            map.put(key, time);
            myRef.updateChildren(map);
//            myRef.push();

            myRef.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
//                    String value = dataSnapshot.getValue(String.class);
                    Log.d(TAG, "Value is: " + dataSnapshot.getKey());
                }

                @Override
                public void onCancelled(DatabaseError error) {
                    Log.w(TAG, "Failed to read value.", error.toException());
                }
            });
        }
    }
    public void onDestroy() {
        super.onDestroy();
        unregisterReceiver(broadcastReceiver);
    }


}