package com.pksms;

import android.content.Context;
import android.telephony.SmsManager;
import android.util.Log;

import java.util.ArrayList;

public class SMSUtil {
    private final static String TAG = SMSUtil.class.getSimpleName();
    public static void sendSMS(Context context, String phoneNo, String message) {
        try {
            // LMS Manager
            SmsManager sms = SmsManager.getDefault();

            // LMS메시지 보내기
            ArrayList<String> parts = sms.divideMessage(message);
            sms.sendMultipartTextMessage(phoneNo, null, parts, null, null);
            Log.i(TAG, "SMS 전송완료");

        } catch (Exception e) {
            Log.e(TAG, "SMS 전송실패");
            e.printStackTrace();
        }
    }
}
