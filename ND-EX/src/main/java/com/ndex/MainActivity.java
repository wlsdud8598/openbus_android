package com.ndex;


import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.webkit.JavascriptInterface;
import android.webkit.JsResult;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.tooth.R;


public class MainActivity extends BaseActivity {
    public final int CUSTOMIZED_REQUEST_CODE = 0x0000ffff;

    private WebView mWebView;
    private LinearLayout mProgressLayout;
    private LinearLayout mIntroLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//            Window window = getWindow();
//            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
//            window.setStatusBarColor(Color.BLACK);   //원하는 배경색깔 지정
//            window.getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR); //원하는 배경색이 흰색일경우 입력, 텍스트색상을 검정색으로 바꿔줍니다.
//
//        }


        mWebView = findViewById(R.id.main_webview);
        mProgressLayout = findViewById(R.id.progress_layout);
        mIntroLayout = findViewById(R.id.intro_layout);


        settingWebview(mWebView);
        mWebView.loadUrl("https://www.nd-exk.com");
//        mWebView.loadUrl("file:///android_asset/javapage.html");
//        try {
//            InputStreamReader is = new InputStreamReader(getAssets().open("javapage.html"));
//            BufferedReader br = new BufferedReader(is);
//            String mLine;
//            while ((mLine = br.readLine()) != null) {
//                //process line
//            }
//
//        } catch (IOException e) {
//            e.printStackTrace();
//        }


    }

    @SuppressLint("JavascriptInterface")
    private void settingWebview(WebView webView){
        webView.getSettings().setJavaScriptEnabled(true);
//        webView.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
        webView.getSettings().setLoadsImagesAutomatically(true);
        webView.getSettings().setUseWideViewPort(true);
        webView.getSettings().setSupportZoom(false);
        webView.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);
        webView.getSettings().setAppCacheEnabled(false);
        webView.getSettings().setDomStorageEnabled(true);
        webView.getSettings().setAllowFileAccess(true);
        webView.setWebChromeClient(new ChromeClient());
        webView.setWebViewClient(new WebClient());
//        webView.getSettings().setUserAgentString("app");
        webView.addJavascriptInterface(new AndroidBridge(), "Android");


    }

    class WebClient extends WebViewClient {
        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);
            mProgressLayout.setVisibility(View.VISIBLE);
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
            mProgressLayout.setVisibility(View.GONE);
            startIntro();
        }

        @Override
        public void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error) {
            super.onReceivedError(view, request, error);
            mProgressLayout.setVisibility(View.GONE);
        }

        @TargetApi(Build.VERSION_CODES.LOLLIPOP)
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {

            String url = request.getUrl() == null ? "" : request.getUrl().toString();

            if (url.startsWith("mailto:") || url.startsWith("tel:")) {
                Intent intent = new Intent(Intent.ACTION_VIEW, request.getUrl());
                startActivity(intent);
                return true;
            } else {
                return super.shouldOverrideUrlLoading(view, request);
            }
        }
    }

    class ChromeClient extends WebChromeClient {

        @Override
        public boolean onJsAlert(WebView view, String url, String message, JsResult result) {
            return super.onJsAlert(view, url, message, result);
        }
    }

    public class AndroidBridge {
        @JavascriptInterface
        public void finishCall(final String message) {
            new Handler().post(new Runnable() {

                public void run() {
                    finishStep(message);
                }
            });
        }
    }

    private void startIntro() {
        if (mIntroLayout != null && mIntroLayout.getVisibility() == View.VISIBLE) {
            Animation anim = AnimationUtils.loadAnimation(MainActivity.this, R.anim.slide_to_left);
            mIntroLayout.startAnimation(anim);
            anim.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {

                }

                @Override
                public void onAnimationEnd(Animation animation) {
                    mIntroLayout.setVisibility(View.GONE);

                }

                @Override
                public void onAnimationRepeat(Animation animation) {

                }
            });
        }

    }


    @Override
    public void onBackPressed() {
        if (mWebView != null && mWebView.canGoBack()) {
            mWebView.goBack();
        } else {
            if (mWebView != null)
                mWebView.loadUrl("javascript:callFinish()");
//            else () {
//
//            }

//            AlertDialogFragment dlg = AlertDialogFragment.newInstance(android.R.drawable.ic_dialog_alert,
//                    "알림",
//                    "종료하시겠습니까?",
//                    true);
//
//            dlg.show(getFragmentManager(), "finish");
//            super.onBackPressed();
        }
    }

    public void finishStep(String message) {
        if (mIsFinishing) {
            mIsFinishing = false;
//            super.onBackPressed();
            finish();
        } else {
            Toast.makeText(MainActivity.this, message, Toast.LENGTH_SHORT).show();
            mIsFinishing = true;
            mMasterHandler.sendEmptyMessageDelayed(HANDLE_FINISHING_EXPIRE, 3000L);
        }
    }

    protected boolean mIsFinishing = false;
    public final int HANDLE_FINISHING_EXPIRE = -999;
    protected Handler mMasterHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case HANDLE_FINISHING_EXPIRE:
                    mIsFinishing = false;
                    break;
            }
        }
    };
}
