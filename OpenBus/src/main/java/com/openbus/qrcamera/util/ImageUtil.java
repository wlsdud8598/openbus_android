package com.openbus.qrcamera.util;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.hardware.camera2.CameraCharacteristics;
import android.media.ExifInterface;
import android.media.Image;
import android.util.Log;

import java.nio.ByteBuffer;

public class ImageUtil {
    private final static String TAG = ImageUtil.class.getSimpleName();

    public static Bitmap convertImageToBitmap(Image image, int facing, int exifOrientation) throws OutOfMemoryError {
        ByteBuffer buffer = image.getPlanes()[0].getBuffer();
        byte[] bytes = new byte[buffer.capacity()];
        buffer.get(bytes);
        Bitmap bitmap = BitmapFactory.decodeByteArray(bytes, 0, bytes.length, null);

        Matrix rotateMatrix = new Matrix();
        Log.i(TAG, "rotation="+exifOrientation);
        rotateMatrix.postRotate(exifOrientationToDegrees(facing, exifOrientation));  // 회전 오류 수정
//            rotateMatrix.postRotate(mOrientation);

        bitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), rotateMatrix, false);

        // 전면카메라일경우 좌우 반전 처리
        if (facing == CameraCharacteristics.LENS_FACING_BACK) {
            Matrix sideInversion = new Matrix();
//                sideInversion.setScale(1, -1);  // 상하반전
            sideInversion.setScale(-1, 1);  // 좌우반전
            bitmap = Bitmap.createBitmap(bitmap, 0, 0,
                    bitmap.getWidth(), bitmap.getHeight(), sideInversion, false);
        } else {

        }
        return bitmap;
    }

    /**
     * 이미지 회전 오류 처리
     * LENS_FACING_FRONT > 후면 카메라
     * @param exifOrientation
     * @return
     */
    public static int exifOrientationToDegrees(int facing, int exifOrientation) {
        if (exifOrientation == ExifInterface.ORIENTATION_ROTATE_90) {
            Log.i(TAG, "exifOrientationToDegrees_90");
            return facing == CameraCharacteristics.LENS_FACING_FRONT ? 180 : 180;
        } else if (exifOrientation == ExifInterface.ORIENTATION_ROTATE_180) {
            Log.i(TAG, "exifOrientationToDegrees_180");
            return facing == CameraCharacteristics.LENS_FACING_FRONT ? 270 : 90;
        } else if (exifOrientation == ExifInterface.ORIENTATION_ROTATE_270) {
            Log.i(TAG, "exifOrientationToDegrees_270");
            return facing == CameraCharacteristics.LENS_FACING_FRONT ? 0 : 0;
        } else if (exifOrientation == ExifInterface.ORIENTATION_NORMAL) {
            Log.i(TAG, "exifOrientationToDegrees_Normal");
            return facing == CameraCharacteristics.LENS_FACING_FRONT ? 90 : 270;
        }
        return 0;
    }
}
