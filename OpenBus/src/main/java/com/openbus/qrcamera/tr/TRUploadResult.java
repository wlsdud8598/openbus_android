package com.openbus.qrcamera.tr;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class TRUploadResult extends BaseData {
	private final String TAG = TRUploadResult.class.getSimpleName();
    @SerializedName("status")
    public String status;
    @SerializedName("message")
    public String message;
    @SerializedName("result")
    public ResultCls result;

    public static class ResultCls implements Parcelable {
        @SerializedName("original")
        public String original;
        @SerializedName("graphic1")
        public String graphic1;
        @SerializedName("graphic2")
        public String graphic2;
        @SerializedName("score")
        public String score;
        @SerializedName("gumscore")
        public String gumscore;
        @SerializedName("is_normal_photo")
        public String is_normal_photo;


        protected ResultCls(Parcel in) {
            original = in.readString();
            graphic1 = in.readString();
            graphic2 = in.readString();
            score = in.readString();
            gumscore = in.readString();
            is_normal_photo = in.readString();
        }

        public static final Creator<ResultCls> CREATOR = new Creator<ResultCls>() {
            @Override
            public ResultCls createFromParcel(Parcel in) {
                return new ResultCls(in);
            }

            @Override
            public ResultCls[] newArray(int size) {
                return new ResultCls[size];
            }
        };

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(original);
            dest.writeString(graphic1);
            dest.writeString(graphic2);
            dest.writeString(score);
            dest.writeString(gumscore);
            dest.writeString(is_normal_photo);
        }
    }

    protected TRUploadResult(Parcel in) {
        status = in.readString();
        message = in.readString();
    }
}
