package com.openbus.qrcamera;


import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.webkit.CookieManager;
import android.webkit.CookieSyncManager;
import android.webkit.JavascriptInterface;
import android.webkit.JsResult;
import android.webkit.WebBackForwardList;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageButton;

import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;
import com.openbus.qrcamera.camera.NewCamera2Activity;
import com.openbus.qrcamera.camera.regacy.CameraActivity;
import com.openbus.qrcamera.tr.TRUploadResult;
import com.openbus.qrcamera.util.SharedPref;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;

/**
 * 치아촬영 인터페이스
 * 닫기
 * 2. 카메라 앵글 열리고 사진촬영
 * 3. 바이너리 이미지를 솔류션 서버에 던짐
 * (솔류션서버 주소는 별도 전달 예정)
 * 4. 솔류션서버에 업로드 되면 json을 리턴함 (이미지주소 2개)
 * { [“http://aaa.com/aa.jpg", “http://aaa.com/aa.jpg"]}
 *
 * 5. 받은 이미지 주소 2개에서 이미지 다운받고
 * 촬영한 사진원본을 포함 총3개와
 * 웹뷰에서 받은 회원아이디, 가족코드를 AWS서버에 던짐
 * 1. 웹뷰에서 카메라를 호출 하는 인터페이스 (웹에서 구현)
 * Native Interface Javascript Code
 *
 * // Android
 * window.Android.toothcamera(“회원아이디”, “가족코드”);

 */

public class MainActivity extends BaseActivity {
    public final int REQUEST_CODE_QR_CODE = 0x0000ffff;
    public static final int REQUEST_CODE_TOOTH_CAMERA = 0x1111;

    private WebView mWebView;

    private String mMenberId;
    private String mFamilyCode;

    private ImageButton mToothBtn;
    private ImageButton mCameraBtn;
    private ImageButton mBackBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mToothBtn = findViewById(R.id.tooth_btn);
        mCameraBtn = findViewById(R.id.camera_btn);
        mBackBtn = findViewById(R.id.back_btn);

        mWebView = findViewById(R.id.main_webview);

        mToothBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                scanBarcode();
                mWebView.loadUrl(Define.URL.CARE_URL);
            }
        });

        mCameraBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Intent intent = new Intent(MainActivity.this, CameraActivity.class);
//                Intent intent = new Intent(MainActivity.this, NewCamera2Activity.class);
//                startActivityForResult(intent, REQUEST_CODE_TOOTH_CAMERA);

            }
        });

        mBackBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        settingWebview(mWebView);
        mWebView.loadUrl(Define.URL.BASE_URL);
//        mWebView.loadUrl("file:///android_asset/javapage.html");
//        try {
//            InputStreamReader is = new InputStreamReader(getAssets().open("javapage.html"));
//            BufferedReader br = new BufferedReader(is);
//            String mLine;
//            while ((mLine = br.readLine()) != null) {
//                //process line
//            }
//
//        } catch (IOException e) {
//            e.printStackTrace();
//        }

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            CookieSyncManager.createInstance(this);
        }

    }

    public void scanBarcode() {
        IntentIntegrator integrator = new IntentIntegrator(MainActivity.this);
        integrator.setCaptureActivity( ZxingActivity.class );
        integrator.setOrientationLocked(false);
        integrator.setRequestCode(REQUEST_CODE_QR_CODE);
        integrator.initiateScan();
    }

    @SuppressLint("JavascriptInterface")
    private void settingWebview(WebView webView){
        webView.getSettings().setJavaScriptEnabled(true);
//        webView.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
        webView.getSettings().setLoadsImagesAutomatically(true);
        webView.getSettings().setUseWideViewPort(true);
        webView.getSettings().setSupportZoom(false);
        webView.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);
        webView.getSettings().setAppCacheEnabled(false);
        webView.getSettings().setDomStorageEnabled(true);
        webView.getSettings().setAllowFileAccess(true);
        webView.setWebChromeClient(new ChromeClient());
        webView.setWebViewClient(new WebClient());
//        webView.getSettings().setUserAgentString("app");
        webView.addJavascriptInterface(new AndroidBridge(), "Android");

        // 웹뷰 디버깅 설정
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            WebView.setWebContentsDebuggingEnabled(BuildConfig.DEBUG);
        }

        // 쿠키 허용 설정
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            mWebView.getSettings().setMixedContentMode(WebSettings.MIXED_CONTENT_ALWAYS_ALLOW);
            CookieManager cookieManager = CookieManager.getInstance();
            cookieManager.setAcceptCookie(true);
            cookieManager.setAcceptThirdPartyCookies(mWebView, true);
        }
    }

    class WebClient extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {

            if (BuildConfig.DEBUG)
                Log.i(TAG, "shouldOverrideUrlLoading.url="+request.getUrl());
            String url = request.getUrl() == null ? "" : request.getUrl().toString();


//            if (url != null) {
//                int sub = url.lastIndexOf("/");
//                if (sub > 0) {
//                    String action = url.substring(sub);
//                    Log.i(TAG, "shouldOverrideUrlLoading.action="+action);
////                    int isVisible = "/login".equals(action) ? View.GONE : View.VISIBLE;
//
////                    mToothBtn.setVisibility(View.GONE );
//
//
//                    if ("/tooth_list".equals(action)) {
//                        mToothBtn.setVisibility(View.VISIBLE );
//                        mCameraBtn.setVisibility(View.VISIBLE );
//                    } else {
//                        mCameraBtn.setVisibility(View.GONE );
//                        mToothBtn.setVisibility(View.GONE );
//                    }
//
////                    if ("/choice".equals(action)) {
////                        mBackBtn.setVisibility(View.VISIBLE );
////                    }
//
//
//                }
//            }

            if (url.startsWith("mailto:") || url.startsWith("tel:")) {
                Intent intent = new Intent(Intent.ACTION_VIEW, request.getUrl());
                startActivity(intent);
                return true;
            } else {
                return super.shouldOverrideUrlLoading(view, request);
            }
        }

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);
            showProgress();
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
            hideProgress();

//            String url = request.getUrl() == null ? "" : request.getUrl().toString();

            if (url != null) {


                setBtnVisible(url);
                Log.i("setBtnVisible : ", url);
//                int sub = url.lastIndexOf("/");
//                if (sub > 0) {
//                    String action = url.substring(sub);
//                    Log.i(TAG, "shouldOverrideUrlLoading.action="+action);
//                }
            }

            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
                CookieSyncManager.getInstance().sync();
            } else {
                CookieManager.getInstance().flush();
            }
        }

        @Override
        public void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error) {
            super.onReceivedError(view, request, error);
            hideProgress();
        }
    }

    class ChromeClient extends WebChromeClient {

        @Override
        public boolean onJsAlert(WebView view, String url, String message, JsResult result) {
            return super.onJsAlert(view, url, message, result);
        }
    }

    public class AndroidBridge {
        @JavascriptInterface
        public void pushservice(final String pushNoti){
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
//                    SharedPref.getInstance().savePreferences(SharedPref.PUSH_NOTI, pushNoti);
//                    Intent intent = new Intent(MainActivity.this, CameraActivity.class);
//                    onActivityResult(REQUEST_CODE_QR_CODE,Activity.RESULT_OK, intent);
                    qrcamera("wlsdud");
                    Log.i(TAG, "push notification =" + pushNoti);
                }
            });
        }

        @JavascriptInterface
        public void memberinfo(final String mberId, final String familyCode) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (mberId != null)
                        mMenberId = mberId;
                    if (familyCode != null)
                        mFamilyCode = familyCode;

                    Log.i(TAG, "memberinfo.mberId="+mberId+", familyCode="+familyCode);
                }
            });
        }

        @JavascriptInterface
        public void toothcamera(final String mberId, final String familyCode) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (mberId != null)
                        mMenberId = mberId;
                    if (familyCode != null)
                        mFamilyCode = familyCode;

                    Log.i(TAG, "toothcamera.mberId="+mberId+", familyCode="+familyCode);
//                    Intent intent = new Intent(MainActivity.this, CameraActivity.class);
                    Intent intent = new Intent(MainActivity.this, NewCamera2Activity.class);
                    startActivityForResult(intent, REQUEST_CODE_TOOTH_CAMERA);
                }
            });
        }

        @JavascriptInterface
        public void qrcamera(final String mberId) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (mberId != null)
                        mMenberId = mberId;
//                    Toast.makeText(MainActivity.this, "qrcamera.mberId="+mberId, Toast.LENGTH_SHORT).show();
                    Log.i(TAG, "qrcamera.mberId="+mberId);
                    new Handler().post(new Runnable() {
                        public void run() {
                            scanBarcode();
                        }
                    });
                }
            });
        }
    }

    private void setBtnVisible(String url) {
        Log.i(TAG, "setBtnVisible.url="+url);
//            - 백버튼은 SNS로그인페이지(m.dentinote.co.kr 도메인이 아닌페이지) 에서만 띄웁니다.
//            그렇지 않으면 백버튼은 항상 숨겨져 있어야 합니다.
        if (url.contains("m.dentinote.co.kr")) {
            mBackBtn.setVisibility(View.GONE);
        } else {
            mBackBtn.setVisibility(View.VISIBLE);
        }
        //- 메인리스트(/users/tooth_list), 상세페이지(users/care/..)를 제외한 카메라버튼은 노출되지 않아야 합니다.
        //  두페이지에서만 노출되고 다른페이지 노촐안되어야 함.
        if (url.contains("/users/tooth_list") || url.contains("/users/care")) {
            mCameraBtn.setVisibility(View.VISIBLE );
        } else {
            mCameraBtn.setVisibility(View.GONE );
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        IntentResult result = IntentIntegrator.parseActivityResult(resultCode, data);
        Log.i(TAG, "onActivityResult.requestCode="+requestCode+", resultCode="+resultCode+", data="+data);
        if (requestCode == REQUEST_CODE_QR_CODE) {
            if (result.getContents() == null) {
                Log.d(TAG, "Cancelled scan");
//                Toast.makeText(this, "Cancelled", Toast.LENGTH_LONG).show();
            } else {
                Log.i(TAG, "Scanned: " + result.getContents());
                String script = "javascript:nativeQRbind(" +
                        "'"+mMenberId+"'" +
                        ", '"+result.getContents()+"'" +
                        ")";
                Log.i(TAG, "script="+script);
                mWebView.loadUrl(script);
            }
        } else if (requestCode == REQUEST_CODE_TOOTH_CAMERA) {
            Log.i(TAG, "onActivityResult.requestCode="+requestCode+", resultCode="+resultCode+", data="+data);
            if (resultCode == Activity.RESULT_OK) {
                TRUploadResult.ResultCls recv = (TRUploadResult.ResultCls) data.getParcelableExtra(Define.INTENT_UPLOAD_RESULT.TRUploadResult);
//                String original = data.getStringExtra(Define.INTENT_UPLOAD_RESULT.ORIGINAL);
//                String graphic1 = data.getStringExtra(Define.INTENT_UPLOAD_RESULT.GRAPHIC1);
//                String graphic2 = data.getStringExtra(Define.INTENT_UPLOAD_RESULT.GRAPHIC2);
//                String score = data.getStringExtra(Define.INTENT_UPLOAD_RESULT.SCORE);
//                String gumScore = data.getStringExtra(Define.INTENT_UPLOAD_RESULT.GUMSCORE);

                String script = "javascript:toothcamera(" +
                        "'"+recv.original+"'" +
                        ", '"+recv.graphic1+"'" +
                        ", '"+recv.graphic2+"'" +
                        ", "+recv.score+
                        ", "+recv.gumscore+
                        ", '"+mMenberId+"'" +
                        ", '"+mFamilyCode+"'" +
                        ", '"+recv.is_normal_photo+"')";
                Log.i(TAG, "script="+script);
                mWebView.loadUrl(script);
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }

    }

    @Override
    public void onBackPressed() {
        if (mWebView != null && mWebView.canGoBack()) {

            WebBackForwardList webBackForwardList = mWebView.copyBackForwardList();
            String currUrl = webBackForwardList.getCurrentItem().getUrl();
            String backUrl = webBackForwardList.getItemAtIndex(webBackForwardList.getCurrentIndex() - 1).getUrl();
            // 1. 현재페이지가 메인(users/tooth_list) 일때 하드웨어, 소프트 백버튼 누르면 항상 사용자선택 페이지(/users/choice)로 이동
            if (currUrl.contains("users/tooth_list") && backUrl.contains("/users/choice") == false) {
                mWebView.clearHistory();
                mWebView.loadUrl(Define.URL.CHOICE_URL);
                setBtnVisible(Define.URL.CHOICE_URL);
            } else {
                setBtnVisible(backUrl);
                mWebView.goBack();
            }
        } else {
            AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
            builder.setMessage("종료하시겠습니까?");
            builder.setPositiveButton("확인", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    MainActivity.super.onBackPressed();
                }
            })
            .setNegativeButton("취소", null)
            .show();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            CookieSyncManager.getInstance().stopSync();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            CookieSyncManager.getInstance().startSync();
        }
    }
}
