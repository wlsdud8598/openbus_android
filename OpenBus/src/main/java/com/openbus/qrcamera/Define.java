package com.openbus.qrcamera;

public class Define {

    public class URL {
        public static final String BASE_URL = "http://elb-stage-mobile-app-272169858.ap-northeast-2.elb.amazonaws.com/";
        public static final String UPLOAD_URL = "http://api.dentinote.co.kr/tooth/processing";  // 사진 업로드
        public static final String CARE_URL = "http://m.dentinote.co.kr/users/care";  // 사진 업로드

        public static final String CHOICE_URL = "http://m.dentinote.co.kr/users/choice";  // 사용자선택
    }

    public class INTENT_UPLOAD_RESULT {
        public static final String TRUploadResult = "TRUploadResult";
//        public static final String ORIGINAL = "ORIGINAL";
//        public static final String GRAPHIC1 = "GRAPHIC1";
//        public static final String GRAPHIC2 = "GRAPHIC2";
//        public static final String SCORE = "score";
//        public static final String GUMSCORE = "gumscore";

    }

}
