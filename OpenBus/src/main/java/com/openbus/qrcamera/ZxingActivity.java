package com.openbus.qrcamera;

import android.os.Bundle;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.widget.TextView;

import com.journeyapps.barcodescanner.CaptureActivity;

/**
 *제품 포장박스에 있는 QR 코드를 찍으면 숫자문자 포함해서
 * “123ADF1231231231AFA23123” 리턴됩니다.
 * 1. 웹뷰에서 QR 호출 하는 인터페이스 (웹에서 구현)
 * Native Interface Javascript Code
 * // Android
 * window.Android.qrcamera();
 * // iOS
 * window.location.href=“interface://qrcamera”;
 * 2. 카메라 앵글 열리고 QR인식됨
 * nativeQRbind(“123ASDF23es0ofa123”);
 */
public class ZxingActivity extends CaptureActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        TextView statusTv = findViewById(R.id.zxing_status_view);
        statusTv.setText("다음에 등록하기");
        statusTv.setTextSize(dpToPixel(7));
        statusTv.setPadding(0,0,0, dpToPixel(10));
        statusTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        Log.i("scan = ","scans");
    }

    public int dpToPixel(int dp){
        int px = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, getResources().getDisplayMetrics());
        return px;
    }
}