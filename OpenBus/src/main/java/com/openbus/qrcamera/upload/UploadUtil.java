package com.openbus.qrcamera.upload;

import android.util.Log;
import android.widget.Toast;

import com.openbus.qrcamera.BaseActivity;
import com.openbus.qrcamera.R;

import java.io.File;

public class UploadUtil {
    private final String TAG = UploadUtil.class.getSimpleName();

    public void doUploadPicture(final BaseActivity activity, File path, final IStep iStep) {
        File filePath = path;         // contentUri.getPath();
        Log.i(TAG, "doUploadPicture.filePath=" + filePath);

        UploadHttpAsyncFileTask rssTask = new UploadHttpAsyncFileTask(new HttpAsyncTaskInterface() {
            @Override
            public void onPreExecute() {
                activity.showProgress_txt();
            }
            @Override
            public void onPostExecute(String data) {
                activity.hideProgress_txt();
            }
            @Override
            public void onError() {
                activity.hideProgress_txt();
                Toast.makeText(activity, activity.getString(R.string.text_network_data_send_fail), Toast.LENGTH_SHORT).show();
            }
            @Override
            public void onFileUploaded(String result) {
                activity.hideProgress_txt();
                iStep.next(result);
            }
        });

        rssTask.setParam(filePath);
        rssTask.execute();
    }

    public interface IStep {
        void next(String result);
    }
}
