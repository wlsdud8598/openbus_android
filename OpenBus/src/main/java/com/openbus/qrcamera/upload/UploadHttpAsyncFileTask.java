package com.openbus.qrcamera.upload;

import android.os.AsyncTask;
import android.util.Log;

import com.openbus.qrcamera.Define;

import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

public class UploadHttpAsyncFileTask extends AsyncTask<String, Void, String> {
    private final String TAG = UploadHttpAsyncFileTask.class.getSimpleName();

    private HttpAsyncTaskInterface atv;
	private static FileInputStream mFileInputStream = null;

	static String lineEnd = "\r\n";
	static String twoHyphens = "--";
	static String boundary = "----WebKitFormBoundaryaQBBslcNnuZfLBro";

	private File mFile;

	public UploadHttpAsyncFileTask(HttpAsyncTaskInterface atv) {
		this.atv = atv;
	}

	@Override
	protected void onPreExecute() {
		atv.onPreExecute();
	}

	@Override
	protected String doInBackground(String... urls) {
        String result = HttpFileUpload(mFile);
        return result;
	}

	@Override
	protected void onPostExecute(String data)  {
		atv.onFileUploaded(data);
	}

	public void setParam(File file) {
		this.mFile = file;
	}

	private String HttpFileUpload(File file) {
		String filePath = file.getPath();
		String result;
		try{
			mFileInputStream = new FileInputStream(filePath);
            URL connectUrl = new URL(Define.URL.UPLOAD_URL);

			HttpURLConnection conn = (HttpURLConnection)connectUrl.openConnection();
			conn.setDoInput(true);
			conn.setDoOutput(true);
			conn.setConnectTimeout(60 * 1000);	// 타임아웃 1분
			conn.setReadTimeout(60 * 1000);		// 타임아웃 1분
			conn.setUseCaches(false);
			conn.setRequestMethod("POST");
			conn.setRequestProperty("Connection", "Keep-Alive");
			conn.setRequestProperty("Content-Type", "multipart/form-data;boundary=" + boundary);

			DataOutputStream dos = new DataOutputStream( conn.getOutputStream()) ;
			dos.writeBytes(twoHyphens + boundary + lineEnd);

			String key = "original";    // key
			dos.writeBytes("Content-Disposition:form-data;name=\"" + key + "\";filename=\"" + filePath + "\"" + lineEnd);
			dos.writeBytes(lineEnd);

			int bytesAvailable = mFileInputStream.available();
			int maxBufferSize = 1024;
			int bufferSize = Math.min(bytesAvailable, maxBufferSize);
			byte[] buffer = new byte[bufferSize];
			int bytesRead = mFileInputStream.read( buffer , 0 , bufferSize);
            Log.i(TAG, "image byte is " + bytesRead );

			while(bytesRead > 0 ){
				dos.write(buffer , 0 , bufferSize);
				bytesAvailable = mFileInputStream.available();
				bufferSize = Math.min(bytesAvailable, maxBufferSize);
				bytesRead = mFileInputStream.read(buffer,0,bufferSize);
			}

			dos.writeBytes(lineEnd);
			dos.writeBytes(twoHyphens + boundary + twoHyphens + lineEnd);

			int serverResponseCode = conn.getResponseCode();
		    String serverResponseMessage = conn.getResponseMessage();

            Log.i(TAG, "HTTP Response is : " + serverResponseMessage + ": " + serverResponseCode);
		    if(serverResponseCode == 200){
		       
		    }

            Log.i(TAG,  "File is written");
			mFileInputStream.close();
			dos.flush(); // 버퍼에 있는 값을 모두 밀어냄

			//웹서버에서 결과를 받아 EditText 컨트롤에 보여줌
			int ch;
			InputStream is = conn.getInputStream();
			StringBuffer b = new StringBuffer();
			while((ch = is.read()) != -1 ){
				b.append((char)ch);
			}

			result = b.toString();
            Log.i(TAG,  "result=" + result);

			dos.close();
		} catch (Exception e) {
			result = null;
			e.printStackTrace();
		}
		return result;
	}
}