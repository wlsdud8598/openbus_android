/*
 * Copyright 2017 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.openbus.qrcamera.camera;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.Surface;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;

import com.google.gson.Gson;
import com.openbus.qrcamera.BaseActivity;
import com.openbus.qrcamera.Define;
import com.openbus.qrcamera.MainActivity;
import com.openbus.qrcamera.R;
import com.openbus.qrcamera.camera.regacy.CameraActivity;
import com.openbus.qrcamera.tr.TRUploadResult;
import com.openbus.qrcamera.upload.UploadUtil;
import com.openbus.qrcamera.util.JsonLogPrint;

import java.io.File;

import androidx.annotation.NonNull;

public class NewResultActivity extends BaseActivity {
    private static final String TAG = NewResultActivity.class.getSimpleName();
    private final static String PICTURE_PATH = "PICTURE_PATH";
    private final static String UPLOAD_PICTURE_PATH = "UPLOAD_PICTURE_PATH";

    private String mPreviewFilePath;
    private String mUploadFilePath;

    private ImageView resultiv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_camera_result);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        try {
            Log.i(TAG, "onCreateView");
            mPreviewFilePath = getIntent().getStringExtra(PICTURE_PATH);
            mUploadFilePath = getIntent().getStringExtra(UPLOAD_PICTURE_PATH);

            Log.i(TAG, "### previewFilePath="+mPreviewFilePath+", uploadFilePath="+mUploadFilePath);

            resultiv = findViewById(R.id.result_iv);
            resultiv.setImageURI(Uri.parse(mPreviewFilePath));

            findViewById(R.id.picture_upload_button).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    doUpload(mUploadFilePath);
                }
            });

            findViewById(R.id.picture_cancel_button).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onBackPressed();
//                reActivity();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            finish();
        }
    }

    @Override
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
    }

    private void doUpload(String path) {
        final File file = new File(path);
        if (file.exists()) {
            // 업로드 중 화면 회전할 경우 Exception 발생 화면 고정 시키기
            int rotation = getWindowManager().getDefaultDisplay().getRotation();
            if (Surface.ROTATION_90 == rotation || Surface.ROTATION_270 == rotation) {
                setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_LANDSCAPE);
            } else {
                setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
            }

            new UploadUtil().doUploadPicture((NewResultActivity.this), file, new UploadUtil.IStep() {
                @Override
                public void next(String result) {
                    Gson gson = new Gson();
                    TRUploadResult trResult = gson.fromJson(result, TRUploadResult.class);

                    if (trResult != null) {
                       try {
                           JsonLogPrint.printJson(TRUploadResult.class.getSimpleName(), result);
                           TRUploadResult.ResultCls resultCls = trResult.result;
                           Log.i(TAG, "### doUpload.Result status="+trResult.status);
                           Log.i(TAG, "### doUpload.Result message="+trResult.message);
                           Log.i(TAG, "### doUpload.Result graphic1="+resultCls.graphic1);

                           Log.i(TAG, "### doUpload.Result score="+resultCls.score);
                           Log.i(TAG, "### doUpload.Result gumscore="+resultCls.gumscore);
                           Log.i(TAG, "### doUpload.Result is_normal_photo="+resultCls.is_normal_photo);

                           final Intent intent = new Intent();
                           intent.putExtra(Define.INTENT_UPLOAD_RESULT.TRUploadResult, resultCls);
//                                intent.putExtra(Define.INTENT_UPLOAD_RESULT.GRAPHIC1, resultCls.graphic1);
//                                intent.putExtra(Define.INTENT_UPLOAD_RESULT.GRAPHIC2, resultCls.graphic2);
//                                intent.putExtra(Define.INTENT_UPLOAD_RESULT.SCORE, resultCls.score);
//                                intent.putExtra(Define.INTENT_UPLOAD_RESULT.GUMSCORE, resultCls.gumscore);


                           setResult(Activity.RESULT_OK, intent);
                           finish();
                       } catch (Exception e) {
                           e.printStackTrace();
                           showFailUploadAlert();
                       }
                    } else {
                        showFailUploadAlert();
                    }

                }
            });
        }
    }

    private void showFailUploadAlert() {
        new AlertDialog.Builder(NewResultActivity.this)
                .setTitle("알림")
                .setIcon(R.mipmap.ic_launcher)
                .setMessage("업로드에 실패 했습니다. 다시 시도해주세요")
                .setPositiveButton("확인", null)
                .setOnDismissListener(new DialogInterface.OnDismissListener() {
                    @Override
                    public void onDismiss(DialogInterface dialog) {
//                                        setResult(Activity.RESULT_OK, intent);
                        finish();
                    }
                })
                .create()
                .show();
    }



//    private void deleteFile() {
//        if (mPreviewFilePath != null) {
//            File file = new File(mPreviewFilePath);
//            if (file.exists()) {
//                file.delete();
//            }
//        }
//    }

    /**
     * 결과 화면 띄우기
     * @param previewFilePath
     * @param uploadFilePath
     */
    public static void startResultActivity(Activity activity, String previewFilePath, String uploadFilePath) {
        Intent intent = new Intent(activity, NewResultActivity.class);
        intent.putExtra(CameraActivity.IS_RESULT, true);
        intent.putExtra(CameraActivity.PICTURE_PATH, previewFilePath);
        intent.putExtra(CameraActivity.UPLOAD_PICTURE_PATH, uploadFilePath);

        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
                | Intent.FLAG_ACTIVITY_SINGLE_TOP);

        Log.i(TAG, "### previewFilePath="+previewFilePath+", uploadFilePath="+uploadFilePath);
//        startActivity(intent);
        activity.startActivityForResult(intent, MainActivity.REQUEST_CODE_TOOTH_CAMERA);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
//        deleteFile();
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        Log.i(TAG, "onSaveInstanceState.outState="+outState);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);

        Log.i(TAG, "onConfigurationChanged.newConfig="+newConfig);
    }
}
