/*
 * Copyright 2017 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.openbus.qrcamera.camera.regacy;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.WindowManager;

import com.openbus.qrcamera.BaseActivity;
import com.openbus.qrcamera.MainActivity;
import com.openbus.qrcamera.R;

import androidx.annotation.Nullable;

public class CameraActivity extends BaseActivity {
    private final String TAG = CameraActivity.class.getSimpleName();

    public static final String IS_RESULT = "IS_RESULT";
    public static final String PICTURE_PATH = "PICTURE_PATH";
    public static final String UPLOAD_PICTURE_PATH = "UPLOAD_PICTURE_PATH";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_camera);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        if (null == savedInstanceState) {

            boolean isResult = getIntent().getBooleanExtra(IS_RESULT, false);
            if (isResult){
                // 결과 화면
                String previewFilePath = getIntent().getStringExtra(PICTURE_PATH);
                String uploadFilePath = getIntent().getStringExtra(UPLOAD_PICTURE_PATH);
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.container, ResultFragment.newInstance(previewFilePath, uploadFilePath))
                        .commit();
            } else {
                // 카메라 화면
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.container, Camera2BasicFragment.newInstance())
                        .commit();
            }

        }
    }

//    @Override
//    public boolean onKeyDown(int keyCode, KeyEvent event) {
//        switch(keyCode) {
//            case KeyEvent.KEYCODE_VOLUME_DOWN:
//            case KeyEvent.KEYCODE_VOLUME_UP:
//                Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.container);
//                if (fragment instanceof Camera2BasicFragment) {
//                    Log.i(TAG, "onKeyDown.fragment=" + fragment);
//                    return ((Camera2BasicFragment) fragment).onKeyDown(keyCode, event);
//                }
//                break;
//            default:
//                super.onKeyDown(keyCode, event);
//                break;
//        }
//        return true;
//    }

    protected void setOrientation(int orientation) {
//        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
//        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        setRequestedOrientation(orientation);
//        if (orientation == ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE) {
//            getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
//        } else {
//            getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
//        }
    }

//    @Override
//    public boolean onKey(View v, int keyCode, KeyEvent event) {
//        if (event.getAction() == KeyEvent.ACTION_UP) {
//            if (keyCode == KeyEvent.KEYCODE_VOLUME_DOWN || keyCode == KeyEvent.KEYCODE_VOLUME_UP) {
//                Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.container);
////            if (fragment instanceof Camera2BasicFragment) {
//                Log.i(TAG, "onKeyDown.fragment="+fragment);
//                return ((Camera2BasicFragment)fragment).onKeyDown(keyCode, event);
////            }
//            }
//        }
//        return false;
//    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

//    public Fragment getVisibleFragment(){
//        FragmentManager fragmentManager = CameraActivity.this.getSupportFragmentManager();
//        List<Fragment> fragments = fragmentManager.getFragments();
//        if(fragments != null){
//            for(Fragment fragment : fragments){
//                if(fragment != null && fragment.isVisible())
//                    return fragment;
//            }
//        }
//        return null;
//    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
//        IntentResult result = IntentIntegrator.parseActivityResult(resultCode, data);
        Log.i(TAG, "onActivityResult.requestCode="+requestCode+", resultCode="+resultCode+", data="+data);
        if (requestCode == MainActivity.REQUEST_CODE_TOOTH_CAMERA) {
            Log.i(TAG, "onActivityResult.requestCode="+requestCode+", resultCode="+resultCode+", data="+data);
            if (resultCode == Activity.RESULT_OK) {

                setResult(Activity.RESULT_OK, data);
                finish();
            }
        }
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);
        Log.i(TAG, "onSaveInstanceState");
        // 액티비티 소멸 전 데이터 저장
//        savedInstanceState.putInt(KEY_INDEX, mIndex);
    }

}
