/*
 * Copyright 2017 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.openbus.qrcamera.camera.regacy;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Surface;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.google.gson.Gson;
import com.openbus.qrcamera.BaseActivity;
import com.openbus.qrcamera.Define;
import com.openbus.qrcamera.R;
import com.openbus.qrcamera.tr.TRUploadResult;
import com.openbus.qrcamera.upload.UploadUtil;
import com.openbus.qrcamera.util.JsonLogPrint;

import java.io.File;

import androidx.annotation.DrawableRes;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

public class ResultFragment extends Fragment {
    private final String TAG = ResultFragment.class.getSimpleName();
    private final static String PICTURE_PATH = "PICTURE_PATH";
    private final static String UPLOAD_PICTURE_PATH = "UPLOAD_PICTURE_PATH";

    private String mPreviewFilePath;
    private String mUploadFilePath;

    public static ResultFragment newInstance(String previewFilePath, String uploadFilePath) {

        Bundle bundle = new Bundle();

        ResultFragment fragment = new ResultFragment();
        bundle.putString(PICTURE_PATH, previewFilePath);
        bundle.putString(UPLOAD_PICTURE_PATH, uploadFilePath);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_camera_result, container, false);
        Log.i(TAG, "onCreateView");
        mPreviewFilePath = getArguments().getString(PICTURE_PATH);
        mUploadFilePath = getArguments().getString(UPLOAD_PICTURE_PATH);

        ImageView resultiv = view.findViewById(R.id.result_iv);
        resultiv.setImageURI(Uri.parse(mPreviewFilePath));

        view.findViewById(R.id.picture_upload_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                doUpload(mUploadFilePath);
            }
        });

        view.findViewById(R.id.picture_cancel_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();
//                reActivity();
            }
        });
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Log.i(TAG, "onCreateView");


    }

    private void doUpload(String path) {
        final File file = new File(path);
        if (file.exists()) {
            // 업로드 중 화면 회전할 경우 Exception 발생 화면 고정 시키기
            int rotation = getActivity().getWindowManager().getDefaultDisplay().getRotation();
            if (Surface.ROTATION_90 == rotation || Surface.ROTATION_270 == rotation) {
                getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_LANDSCAPE);
            } else {
                getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
            }

            new UploadUtil().doUploadPicture(((BaseActivity)getActivity()), file, new UploadUtil.IStep() {
                @Override
                public void next(String result) {
                    Gson gson = new Gson();
                    TRUploadResult trResult = gson.fromJson(result, TRUploadResult.class);

                    if (trResult != null) {
                       try {
                           JsonLogPrint.printJson(TRUploadResult.class.getSimpleName(), result);
                           TRUploadResult.ResultCls resultCls = trResult.result;
                           Log.i(TAG, "### doUpload.Result status="+trResult.status);
                           Log.i(TAG, "### doUpload.Result message="+trResult.message);
                           Log.i(TAG, "### doUpload.Result graphic1="+resultCls.graphic1);

                           Log.i(TAG, "### doUpload.Result score="+resultCls.score);
                           Log.i(TAG, "### doUpload.Result gumscore="+resultCls.gumscore);
                           Log.i(TAG, "### doUpload.Result is_normal_photo="+resultCls.is_normal_photo);

                           final Intent intent = new Intent();
                           intent.putExtra(Define.INTENT_UPLOAD_RESULT.TRUploadResult, resultCls);
//                                intent.putExtra(Define.INTENT_UPLOAD_RESULT.GRAPHIC1, resultCls.graphic1);
//                                intent.putExtra(Define.INTENT_UPLOAD_RESULT.GRAPHIC2, resultCls.graphic2);
//                                intent.putExtra(Define.INTENT_UPLOAD_RESULT.SCORE, resultCls.score);
//                                intent.putExtra(Define.INTENT_UPLOAD_RESULT.GUMSCORE, resultCls.gumscore);


                           getActivity().setResult(Activity.RESULT_OK, intent);
                           getActivity().finish();
                       } catch (Exception e) {
                           e.printStackTrace();
                           showFailUploadAlert();
                       }
                    } else {
                        showFailUploadAlert();
                    }

                }
            });
        }
    }

    private void showFailUploadAlert() {
        new AlertDialog.Builder(getActivity())
                .setTitle("알림")
                .setIcon(R.mipmap.ic_launcher)
                .setMessage("업로드에 실패 했습니다. 다시 시도해주세요")
                .setPositiveButton("확인", null)
                .setOnDismissListener(new DialogInterface.OnDismissListener() {
                    @Override
                    public void onDismiss(DialogInterface dialog) {
//                                        getActivity().setResult(Activity.RESULT_OK, intent);
                        getActivity().finish();
                    }
                })
                .create()
                .show();
    }


    /**
     * Alert Dialog Fragment
     */
    public static class AlertDialogFragment extends DialogFragment {
        public static BaseActivity.AlertDialogFragment newInstance(@DrawableRes int iconId, CharSequence title, CharSequence message, boolean finishActivity) {
            BaseActivity.AlertDialogFragment fragment = new BaseActivity.AlertDialogFragment();

            Bundle args = new Bundle();
            args.putInt("icon", iconId);
            args.putCharSequence("title", title);
            args.putCharSequence("message", message);
            args.putBoolean("finish_activity", finishActivity);

            fragment.setArguments(args);
            return fragment;
        }

        @NonNull
        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            boolean finishActivity = getArguments().getBoolean("finish_activity");

            return new AlertDialog.Builder(getActivity())
                    .setTitle(getArguments().getCharSequence("title"))
                    .setIcon(getArguments().getInt("icon"))
                    .setMessage(getArguments().getCharSequence("message"))
                    .setPositiveButton(android.R.string.ok, finishActivity ?
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                    getActivity().finish();
                                }
                            } : null)
                    .setCancelable(false)
                    .create();
        }
    }


//    private void deleteFile() {
//        if (mPreviewFilePath != null) {
//            File file = new File(mPreviewFilePath);
//            if (file.exists()) {
//                file.delete();
//            }
//        }
//    }

    @Override
    public void onDestroy() {
        super.onDestroy();
//        deleteFile();
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        Log.i(TAG, "onSaveInstanceState.outState="+outState);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);

        Log.i(TAG, "onConfigurationChanged.newConfig="+newConfig);
    }
}
