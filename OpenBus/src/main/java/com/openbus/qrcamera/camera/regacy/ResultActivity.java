/*
 * Copyright 2017 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.openbus.qrcamera.camera.regacy;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.WindowManager;

import com.openbus.qrcamera.BaseActivity;
import com.openbus.qrcamera.MainActivity;
import com.openbus.qrcamera.R;

public class ResultActivity extends BaseActivity {
    private static final String TAG = ResultActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_camera);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        if (null == savedInstanceState) {

            boolean isResult = getIntent().getBooleanExtra(CameraActivity.IS_RESULT, false);
            if (isResult){
                // 결과 화면
                String previewFilePath = getIntent().getStringExtra(CameraActivity.PICTURE_PATH);
                String uploadFilePath = getIntent().getStringExtra(CameraActivity.UPLOAD_PICTURE_PATH);
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.container, ResultFragment.newInstance(previewFilePath, uploadFilePath))
                        .commit();
            } else {
                // 카메라 화면
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.container, Camera2BasicFragment.newInstance())
                        .commit();
            }

        }
    }

    /**
     * 결과 화면 띄우기
     * @param previewFilePath
     * @param uploadFilePath
     */
    public static void startResultActivity(Activity activity, String previewFilePath, String uploadFilePath) {
        Intent intent = new Intent(activity, ResultActivity.class);
        intent.putExtra(CameraActivity.IS_RESULT, true);
        intent.putExtra(CameraActivity.PICTURE_PATH, previewFilePath);
        intent.putExtra(CameraActivity.UPLOAD_PICTURE_PATH, uploadFilePath);

        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
                | Intent.FLAG_ACTIVITY_SINGLE_TOP);

        Log.i(TAG, "### previewFilePath="+previewFilePath+", uploadFilePath="+uploadFilePath);
//        startActivity(intent);
        activity.startActivityForResult(intent, MainActivity.REQUEST_CODE_TOOTH_CAMERA);
    }


    protected void setOrientation(int orientation) {
//        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
//        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        setRequestedOrientation(orientation);
//        if (orientation == ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE) {
//            getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
//        } else {
//            getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
//        }
    }

    @Override
    public void onBackPressed() {
//        if (getVisibleFragment() instanceof ResultFragment) {
//            ((ResultFragment)getVisibleFragment()).reActivity();
//        } else {
            super.onBackPressed();
//        }
    }

//    public Fragment getVisibleFragment(){
//        FragmentManager fragmentManager = ResultActivity.this.getSupportFragmentManager();
//        List<Fragment> fragments = fragmentManager.getFragments();
//        if(fragments != null){
//            for(Fragment fragment : fragments){
//                if(fragment != null && fragment.isVisible())
//                    return fragment;
//            }
//        }
//        return null;
//    }


    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);
        Log.i(TAG, "onSaveInstanceState");
        // 액티비티 소멸 전 데이터 저장
//        savedInstanceState.putInt(KEY_INDEX, mIndex);
    }

}
