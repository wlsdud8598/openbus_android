/*
 * Copyright 2017 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.openbus.qrcamera.camera;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.ImageFormat;
import android.graphics.Matrix;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.SurfaceTexture;
import android.hardware.SensorManager;
import android.hardware.camera2.CameraAccessException;
import android.hardware.camera2.CameraCaptureSession;
import android.hardware.camera2.CameraCharacteristics;
import android.hardware.camera2.CameraDevice;
import android.hardware.camera2.CameraManager;
import android.hardware.camera2.CameraMetadata;
import android.hardware.camera2.CaptureFailure;
import android.hardware.camera2.CaptureRequest;
import android.hardware.camera2.CaptureResult;
import android.hardware.camera2.TotalCaptureResult;
import android.hardware.camera2.params.MeteringRectangle;
import android.hardware.camera2.params.StreamConfigurationMap;
import android.media.ExifInterface;
import android.media.Image;
import android.media.ImageReader;
import android.media.MediaActionSound;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.util.Log;
import android.util.Size;
import android.util.SparseIntArray;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.OrientationEventListener;
import android.view.Surface;
import android.view.TextureView;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.openbus.qrcamera.BaseActivity;
import com.openbus.qrcamera.BuildConfig;
import com.openbus.qrcamera.MainActivity;
import com.openbus.qrcamera.R;
import com.openbus.qrcamera.util.SharedPref;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

public class NewCamera2Activity extends BaseActivity implements View.OnClickListener, ActivityCompat.OnRequestPermissionsResultCallback {
    /**
     * Conversion from screen rotation to JPEG orientation.
     */
    private static final SparseIntArray ORIENTATIONS = new SparseIntArray();
    private static final int REQUEST_CAMERA_PERMISSION = 1;
    private static final String FRAGMENT_DIALOG = "dialog";

    public static final String IS_RESULT = "IS_RESULT";
    public static final String PICTURE_PATH = "PICTURE_PATH";
    public static final String UPLOAD_PICTURE_PATH = "UPLOAD_PICTURE_PATH";

    private Integer mFacing = CameraCharacteristics.LENS_FACING_FRONT;  // 카메라 전 후면 값.(기본값 후면 Front가 후면)

    static {
        ORIENTATIONS.append(Surface.ROTATION_0, 90);
        ORIENTATIONS.append(Surface.ROTATION_90, 0);
        ORIENTATIONS.append(Surface.ROTATION_180, 270);
        ORIENTATIONS.append(Surface.ROTATION_270, 180);
    }

    private static final String TAG = "Camera2BasicFragment";
    private static final int STATE_PREVIEW = 0;
    private static final int STATE_WAITING_LOCK = 1;
    private static final int STATE_WAITING_PRECAPTURE = 2;
    private static final int STATE_WAITING_NON_PRECAPTURE = 3;
    private static final int STATE_PICTURE_TAKEN = 4;
    private static final int MAX_PREVIEW_WIDTH = 1920;
    private static final int MAX_PREVIEW_HEIGHT = 1080;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_camera2_basic);

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        if (null == savedInstanceState) {

            boolean isResult = getIntent().getBooleanExtra(IS_RESULT, false);
            String previewFilePath = getIntent().getStringExtra(PICTURE_PATH);
            String uploadFilePath = getIntent().getStringExtra(UPLOAD_PICTURE_PATH);
//            if (isResult){
//                // 결과 화면
//                String previewFilePath = getIntent().getStringExtra(PICTURE_PATH);
//                String uploadFilePath = getIntent().getStringExtra(UPLOAD_PICTURE_PATH);
//                getSupportFragmentManager().beginTransaction()
//                        .replace(R.id.container, ResultFragment.newInstance(previewFilePath, uploadFilePath))
//                        .commit();
//            } else {
//                // 카메라 화면
//                getSupportFragmentManager().beginTransaction()
//                        .replace(R.id.container, Camera2BasicFragment.newInstance())
//                        .commit();
//            }

        }


        mFacing = SharedPref.getInstance(NewCamera2Activity.this).getPreferences(SharedPref.CAMERA_FACING, CameraCharacteristics.LENS_FACING_FRONT);

        findViewById(R.id.camera_finish_btn).setOnClickListener(this);
        mShutterBtn = findViewById(R.id.shutter_btn);
        mShutterBtn.setEnabled(false);
        mShutterBtn.setOnClickListener(this);
        findViewById(R.id.facing_btn).setOnClickListener(this);
        mTextureView = findViewById(R.id.texture);
        mGuideLayout = findViewById(R.id.guide_panel_layout);

        mTextureView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent motionEvent) {
//                return onZoomTouch(v, event);
//                return false;
                try {
                    final int actionMasked = motionEvent.getActionMasked();
                    if (actionMasked != MotionEvent.ACTION_DOWN) {
                        return false;
                    }

                    CameraManager manager = (CameraManager) NewCamera2Activity.this.getSystemService(Context.CAMERA_SERVICE);
                    CameraCharacteristics mCameraInfo = manager.getCameraCharacteristics(mCameraId);

                    if (mManualFocusEngaged) {
                        Log.d(TAG, "touch Manual focus already engaged");
                        return true;
                    }
                    final Rect sensorArraySize = mCameraInfo.get(CameraCharacteristics.SENSOR_INFO_ACTIVE_ARRAY_SIZE);

                    //TODO: here I just flip x,y, but this needs to correspond with the sensor orientation (via SENSOR_ORIENTATION)
                    final int y = (int) ((motionEvent.getX() / (float) v.getWidth()) * (float) sensorArraySize.height());
                    final int x = (int) ((motionEvent.getY() / (float) v.getHeight()) * (float) sensorArraySize.width());
//                    final int halfTouchWidth  = 150; //(int)motionEvent.getTouchMajor(); //TODO: this doesn't represent actual touch size in pixel. Values range in [3, 10]...
//                    final int halfTouchHeight = 150; //(int)motionEvent.getTouchMinor();

                    final int halfTouchWidth = (int) motionEvent.getTouchMajor(); //TODO: this doesn't represent actual touch size in pixel. Values range in [3, 10]...
                    final int halfTouchHeight = (int) motionEvent.getTouchMinor();

                    Log.d(TAG, "touch halfTouchWidth=" + halfTouchWidth + ", halfTouchHeight=" + halfTouchHeight);
//                    MeteringRectangle focusAreaTouch = new MeteringRectangle(Math.max(x - halfTouchWidth,  0),
//                            Math.max(y - halfTouchHeight, 0),
//                            halfTouchWidth  * 2,
//                            halfTouchHeight * 2,
//                            MeteringRectangle.METERING_WEIGHT_MAX - 1);

                    MeteringRectangle focusAreaTouch = onAreaTouchEvent(mCameraInfo, motionEvent);

                    CameraCaptureSession.CaptureCallback captureCallbackHandler = new CameraCaptureSession.CaptureCallback() {
                        @Override
                        public void onCaptureCompleted(CameraCaptureSession session, CaptureRequest request, TotalCaptureResult result) {
                            super.onCaptureCompleted(session, request, result);
                            mManualFocusEngaged = false;
//                            Log.d(TAG, "touch onCaptureCompleted");
                            if (request.getTag() == "FOCUS_TAG") {
                                //the focus trigger is complete -
                                //resume repeating (preview surface will get frames), clear AF trigger
                                mPreviewRequestBuilder.set(CaptureRequest.CONTROL_AF_TRIGGER, null);
//                                try {
//                                    mCaptureSession.setRepeatingRequest(mPreviewRequest, mCaptureCallback, mBackgroundHandler);
//                                } catch (CameraAccessException e) {
//                                    e.printStackTrace();
//                                }
                            }

                            // Auto focus should be continuous for camera preview.
                            mPreviewRequestBuilder.set(CaptureRequest.CONTROL_AF_MODE, CaptureRequest.CONTROL_AF_MODE_CONTINUOUS_PICTURE);
                            // Flash is automatically enabled when necessary.
                            setAutoFlash(mPreviewRequestBuilder);

                            // Finally, we start displaying the camera preview.
//                            mPreviewRequest = mPreviewRequestBuilder.build();
//                            mCaptureSession.setRepeatingRequest(mPreviewRequest, mCaptureCallback, mBackgroundHandler);

                            unlockFocus();
                        }

                        @Override
                        public void onCaptureFailed(CameraCaptureSession session, CaptureRequest request, CaptureFailure failure) {
                            super.onCaptureFailed(session, request, failure);
                            Log.d(TAG, "touch AF failure: " + failure);
                            mManualFocusEngaged = false;

                            mPreviewRequestBuilder.set(CaptureRequest.CONTROL_AF_MODE, CaptureRequest.CONTROL_AF_MODE_CONTINUOUS_PICTURE);
                            // Flash is automatically enabled when necessary.
                            setAutoFlash(mPreviewRequestBuilder);

                            // Finally, we start displaying the camera preview.
//                            mPreviewRequest = mPreviewRequestBuilder.build();
//                            mCaptureSession.setRepeatingRequest(mPreviewRequest, mCaptureCallback, mBackgroundHandler);

                            unlockFocus();
                        }
                    };

                    //first stop the existing repeating request
                    mCaptureSession.stopRepeating();

                    //cancel any existing AF trigger (repeated touches, etc.)
                    mPreviewRequestBuilder.set(CaptureRequest.CONTROL_AF_TRIGGER, CameraMetadata.CONTROL_AF_TRIGGER_CANCEL);
                    mPreviewRequestBuilder.set(CaptureRequest.CONTROL_AF_MODE, CaptureRequest.CONTROL_AF_MODE_OFF);
                    mCaptureSession.capture(mPreviewRequestBuilder.build(), captureCallbackHandler, mBackgroundHandler);

                    //Now add a new AF trigger with focus region
                    if (isMeteringAreaAFSupported(mCameraInfo)) {
                        mPreviewRequestBuilder.set(CaptureRequest.CONTROL_AF_REGIONS, new MeteringRectangle[]{focusAreaTouch});
                    }
                    mPreviewRequestBuilder.set(CaptureRequest.CONTROL_MODE, CameraMetadata.CONTROL_MODE_AUTO);
                    mPreviewRequestBuilder.set(CaptureRequest.CONTROL_AF_MODE, CaptureRequest.CONTROL_AF_MODE_AUTO);
                    mPreviewRequestBuilder.set(CaptureRequest.CONTROL_AF_TRIGGER, CameraMetadata.CONTROL_AF_TRIGGER_START);
                    mPreviewRequestBuilder.setTag("FOCUS_TAG"); //we'll capture this later for resuming the preview

                    //then we ask for a single request (not repeating!)
                    mCaptureSession.capture(mPreviewRequestBuilder.build(), captureCallbackHandler, mBackgroundHandler);
                    mManualFocusEngaged = true;
                } catch (CameraAccessException e) {
                    e.printStackTrace();
                    return false;
                }
                return true;
            }
        });

    }

    /**
     * {@link TextureView.SurfaceTextureListener} handles several lifecycle events on a
     * {@link TextureView}.
     */
    private final TextureView.SurfaceTextureListener mSurfaceTextureListener = new TextureView.SurfaceTextureListener() {

        @Override
        public void onSurfaceTextureAvailable(SurfaceTexture texture, int width, int height) {
            openCamera(width, height, mFacing);
        }

        @Override
        public void onSurfaceTextureSizeChanged(SurfaceTexture texture, int width, int height) {
            configureTransform(width, height);
        }

        @Override
        public boolean onSurfaceTextureDestroyed(SurfaceTexture texture) {
            return true;
        }

        @Override
        public void onSurfaceTextureUpdated(SurfaceTexture texture) {
//            Log.i(TAG, "onSurfaceTextureUpdated");
        }
    };

    private String mCameraId;

    private AutoFitTextureView mTextureView;
    private LinearLayout mGuideLayout;
    private CameraCaptureSession mCaptureSession;
    private CameraDevice mCameraDevice;
    private Size mPreviewSize;

    private final CameraDevice.StateCallback mStateCallback = new CameraDevice.StateCallback() {

        @Override
        public void onOpened(@NonNull CameraDevice cameraDevice) {
            // This method is called when the camera is opened.  We start camera preview here.
            mCameraOpenCloseLock.release();
            mCameraDevice = cameraDevice;
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    createCameraPreviewSession();

                }
            });
        }

        @Override
        public void onClosed(@NonNull CameraDevice camera) {
            super.onClosed(camera);
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    mShutterBtn.setEnabled(false);
                }
            });
        }

        @Override
        public void onDisconnected(@NonNull CameraDevice cameraDevice) {
            mCameraOpenCloseLock.release();
            cameraDevice.close();
            mCameraDevice = null;
        }

        @Override
        public void onError(@NonNull CameraDevice cameraDevice, int error) {
            mCameraOpenCloseLock.release();
            cameraDevice.close();
            mCameraDevice = null;
                finish();
        }
    };

    private HandlerThread mBackgroundThread;
    private Handler mBackgroundHandler;
    private ImageReader mImageReader;
    private File mFile;
    private File mUploadFile;
    private final ImageReader.OnImageAvailableListener mOnImageAvailableListener = new ImageReader.OnImageAvailableListener() {

        @Override
        public void onImageAvailable(ImageReader reader) {
            mBackgroundHandler.post(new ImageSaver(reader.acquireNextImage(), mFile));
        }
    };
    private CaptureRequest.Builder mPreviewRequestBuilder;
    private CaptureRequest mPreviewRequest;
    private int mState = STATE_PREVIEW;
    private Semaphore mCameraOpenCloseLock = new Semaphore(1);
    private boolean mFlashSupported;
    private int mSensorOrientation;
    private CameraCaptureSession.CaptureCallback mCaptureCallback = new CameraCaptureSession.CaptureCallback() {

        private void process(CaptureResult result) {

            switch (mState) {
                case STATE_PREVIEW: {
                    // We have nothing to do when the camera preview is working normally.
                    break;
                }
                case STATE_WAITING_LOCK: {

                    Integer afState = result.get(CaptureResult.CONTROL_AF_STATE);
                    Log.i(TAG, "mCaptureCallback.process.STATE_WAITING_LOCK=" + mState + ", afState=" + afState);
                    if (afState == null) {
                        captureStillPicture();
                    } else if (CaptureResult.CONTROL_AF_STATE_FOCUSED_LOCKED == afState ||
                            CaptureResult.CONTROL_AF_STATE_NOT_FOCUSED_LOCKED == afState) {
                        // CONTROL_AE_STATE can be null on some devices
                        Integer aeState = result.get(CaptureResult.CONTROL_AE_STATE);
                        if (aeState == null || aeState == CaptureResult.CONTROL_AE_STATE_CONVERGED) {
                            mState = STATE_PICTURE_TAKEN;
                            captureStillPicture();
                        } else {
                            runPrecaptureSequence();
                        }
                    } else {
                        if (mFacing == CameraCharacteristics.LENS_FACING_BACK) {
                            // CONTROL_AE_STATE can be null on some devices
                            mState = STATE_PICTURE_TAKEN;
                            captureStillPicture();
//                            Integer aeState = result.get(CaptureResult.CONTROL_AE_STATE);
//                            if (aeState == null || aeState == CaptureResult.CONTROL_AE_STATE_CONVERGED) {
//                                mState = STATE_PICTURE_TAKEN;
//                                captureStillPicture();
//                            } else {
//                                runPrecaptureSequence();
//                            }
                        }
                    }
                    break;
                }
                case STATE_WAITING_PRECAPTURE: {
                    // CONTROL_AE_STATE can be null on some devices
                    Integer aeState = result.get(CaptureResult.CONTROL_AE_STATE);
                    if (aeState == null ||
                            aeState == CaptureResult.CONTROL_AE_STATE_PRECAPTURE ||
                            aeState == CaptureRequest.CONTROL_AE_STATE_FLASH_REQUIRED) {
                        mState = STATE_WAITING_NON_PRECAPTURE;
                    }
                    break;
                }
                case STATE_WAITING_NON_PRECAPTURE: {
                    // CONTROL_AE_STATE can be null on some devices
                    Integer aeState = result.get(CaptureResult.CONTROL_AE_STATE);
                    if (aeState == null || aeState != CaptureResult.CONTROL_AE_STATE_PRECAPTURE) {
                        mState = STATE_PICTURE_TAKEN;
                        captureStillPicture();
                    }
                    break;
                }
            }
        }

        @Override
        public void onCaptureProgressed(@NonNull CameraCaptureSession session,
                                        @NonNull CaptureRequest request,
                                        @NonNull CaptureResult partialResult) {
            process(partialResult);
        }

        @Override
        public void onCaptureCompleted(@NonNull CameraCaptureSession session,
                                       @NonNull CaptureRequest request,
                                       @NonNull TotalCaptureResult result) {
            process(result);
        }

    };

    /**
     * Shows a {@link Toast} on the UI thread.
     *
     * @param text The message to show
     */
    private void showToast(final String text) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(NewCamera2Activity.this, text, Toast.LENGTH_SHORT).show();
            }
        });
    }

    /**
     * Given {@code choices} of {@code Size}s supported by a camera, choose the smallest one that
     * is at least as large as the respective texture view size, and that is at most as large as the
     * respective max size, and whose aspect ratio matches with the specified value. If such size
     * doesn't exist, choose the largest one that is at most as large as the respective max size,
     * and whose aspect ratio matches with the specified value.
     *
     * @param choices           The list of sizes that the camera supports for the intended output
     *                          class
     * @param textureViewWidth  The width of the texture view relative to sensor coordinate
     * @param textureViewHeight The height of the texture view relative to sensor coordinate
     * @param maxWidth          The maximum width that can be chosen
     * @param maxHeight         The maximum height that can be chosen
     * @param aspectRatio       The aspect ratio
     * @return The optimal {@code Size}, or an arbitrary one if none were big enough
     */
    private static Size chooseOptimalSize(Size[] choices, int textureViewWidth,
                                          int textureViewHeight, int maxWidth, int maxHeight, Size aspectRatio) {

        // Collect the supported resolutions that are at least as big as the preview Surface
        List<Size> bigEnough = new ArrayList<>();
        // Collect the supported resolutions that are smaller than the preview Surface
        List<Size> notBigEnough = new ArrayList<>();
        int w = aspectRatio.getWidth();
        int h = aspectRatio.getHeight();
        for (Size option : choices) {
            if (option.getWidth() <= maxWidth && option.getHeight() <= maxHeight &&
                    option.getHeight() == option.getWidth() * h / w) {
                if (option.getWidth() >= textureViewWidth &&
                        option.getHeight() >= textureViewHeight) {
                    bigEnough.add(option);
                } else {
                    notBigEnough.add(option);
                }
            }
        }

        if (bigEnough.size() > 0) {
            return Collections.min(bigEnough, new CompareSizesByArea());
        } else if (notBigEnough.size() > 0) {
            return Collections.max(notBigEnough, new CompareSizesByArea());
        } else {
            Log.e(TAG, "Couldn't find any suitable preview size");
            return choices[0];
        }
    }

    /**
     * 4:3 사이즈 이미지
     *
     * @param choices
     * @return
     */
    private static Size chooseVideoSize(Size[] choices) {
        for (Size size : choices) {
            if (size.getWidth() == size.getHeight() * 4 / 3 && size.getWidth() <= 1080) {
                return size;
            }
        }
        Log.e(TAG, "Couldn't find any suitable video size");
        return choices[choices.length - 1];
    }



    private boolean mManualFocusEngaged = false;

    private OrientationEventListener orientEventListener;
    private ImageButton mShutterBtn;

    private boolean isMeteringAreaAFSupported(CameraCharacteristics info) {
        return info.get(CameraCharacteristics.CONTROL_MAX_REGIONS_AF) >= 1;
    }

    public MeteringRectangle onAreaTouchEvent(CameraCharacteristics mCameraInfo, MotionEvent event) {
        Rect rect = mCameraInfo.get(CameraCharacteristics.SENSOR_INFO_ACTIVE_ARRAY_SIZE);
        Log.i("onAreaTouchEvent", "SENSOR_INFO_ACTIVE_ARRAY_SIZE,,,,,,,,rect.left--->" + rect.left + ",,,rect.top--->" + rect.top + ",,,,rect.right--->" + rect.right + ",,,,rect.bottom---->" + rect.bottom);
        Size size = mCameraInfo.get(CameraCharacteristics.SENSOR_INFO_PIXEL_ARRAY_SIZE);
        Log.i("onAreaTouchEvent", "mCameraCharacteristics,,,,size.getWidth()--->" + size.getWidth() + ",,,size.getHeight()--->" + size.getHeight());
        int areaSize = 200;
        int right = rect.right;
        int bottom = rect.bottom;
        int viewWidth = mTextureView.getWidth();
        int viewHeight = mTextureView.getHeight();
        int ll, rr;
        Rect newRect;
        int centerX = (int) event.getX();
        int centerY = (int) event.getY();
        ll = ((centerX * right) - areaSize) / viewWidth;
        rr = ((centerY * bottom) - areaSize) / viewHeight;
        int focusLeft = clamp(ll, 0, right);
        int focusBottom = clamp(rr, 0, bottom);
        Log.i("focus_position", "focusLeft--->" + focusLeft + ",,,focusTop--->" + focusBottom + ",,,focusRight--->" + (focusLeft + areaSize) + ",,,focusBottom--->" + (focusBottom + areaSize));
        newRect = new Rect(focusLeft, focusBottom, focusLeft + areaSize, focusBottom + areaSize);
        MeteringRectangle meteringRectangle = new MeteringRectangle(newRect, 500);
        MeteringRectangle[] meteringRectangleArr = {meteringRectangle};

        return meteringRectangle;
    }

    /**
     * 触摸对焦的计算
     *
     * @param x
     * @param min
     * @param max
     * @return
     */
    private int clamp(int x, int min, int max) {
        if (x < min) {
            return min;
        } else if (x > max) {
            return max;
        } else {
            return x;
        }
    }

    int mOrientation = 0;

    private void setOrientationSensor() {
        if (orientEventListener == null) {
            //보통 속도(SENSOR_DELAY_NORMAL)로 측정
            orientEventListener = new OrientationEventListener(NewCamera2Activity.this, SensorManager.SENSOR_DELAY_NORMAL) {
                @Override
                public void onOrientationChanged(int arg0) {
//                mTextOrient.setText("Orientation: "+ String.valueOf(arg0));

                    // 0˚ (portrait)
                    if (arg0 >= 315 || arg0 < 45) {
//                    mTextArrow.setRotation(0);
                        mOrientation = ExifInterface.ORIENTATION_NORMAL;
                    }
                    // 90˚
                    else if (arg0 >= 45 && arg0 < 135) {
//                    mTextArrow.setRotation(270);
                        mOrientation = ExifInterface.ORIENTATION_ROTATE_90;
                    }
                    // 180˚
                    else if (arg0 >= 135 && arg0 < 225) {
//                    mTextArrow.setRotation(180);
                        mOrientation = ExifInterface.ORIENTATION_ROTATE_180;
                    }
                    // 270˚ (landscape)
                    else if (arg0 >= 225 && arg0 < 315) {
//                    mTextArrow.setRotation(90);
                        mOrientation = ExifInterface.ORIENTATION_ROTATE_270;
                    }

                    // 미리보기 화면 가로 모드에서 바로 회전하여 반대로 회전 할 경우 미리보기 화면 뒤집어짐 오류 수정
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
//                            Log.i(TAG, "onOrientationChanged.mOrientation270="+mOrientation+", mConfigurationRotation="+mConfigurationRotation);
                            if (Surface.ROTATION_90 == mConfigurationRotation && mOrientation == ExifInterface.ORIENTATION_ROTATE_270) {
                                if (mTextureView.getRotation() != 0)
                                    mTextureView.setRotation(0);
                            }

                            if (Surface.ROTATION_90 == mConfigurationRotation && mOrientation == ExifInterface.ORIENTATION_ROTATE_90) {
                                if (mTextureView.getRotation() != 180)
                                    mTextureView.setRotation(180);
                            }

                            if (Surface.ROTATION_270 == mConfigurationRotation && mOrientation == ExifInterface.ORIENTATION_ROTATE_90) {
                                if (mTextureView.getRotation() != 0)
                                    mTextureView.setRotation(0);
                            }

                            if (Surface.ROTATION_270 == mConfigurationRotation && mOrientation == ExifInterface.ORIENTATION_ROTATE_270) {
                                if (mTextureView.getRotation() != 180)
                                    mTextureView.setRotation(180);
                            }
                        }
                    }, 1000);
                }
            };
        }

        //핸들러 활성화
        orientEventListener.enable();
        //인식 오류 시, Toast 메시지 출력
        if (!orientEventListener.canDetectOrientation()) {
            Toast.makeText(NewCamera2Activity.this, "Can't DetectOrientation", Toast.LENGTH_LONG).show();
        }
    }


//    @Override
//    public void onActivityCreated(Bundle savedInstanceState) {
//        super.onActivityCreated(savedInstanceState);
////        createSaveFile();
//    }

    /**
     * Preview용 파일과 업로드용 파일을 생성 한다.
     */
    private void createSaveFile() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
        String now = sdf.format(new Date());

        String path = NewCamera2Activity.this.getCacheDir().getPath();

//        if (BuildConfig.DEBUG) {
//             path = Environment.getExternalStorageDirectory()
//                     +File.separator
//                     +Environment.DIRECTORY_DCIM
//                     +"/Camera";
//        }

        mFile = new File(path, now + ".jpg");

        if (mFacing == CameraCharacteristics.LENS_FACING_FRONT) {
            mUploadFile = new File(path, now + "_upload.jpg");    // 후면카메라인경우(반전처리용 이미지 경로)
        } else {
            mUploadFile = new File(mFile.getPath());                   // 전면카메라인경우
        }

        Log.i(TAG, "mFile.getAbsolutePath()=" + mFile.getAbsolutePath());

        if (new File(path).exists() == false) {
            new File(path).mkdirs();
            Log.i(TAG, "path.mkdirs=" + path);
        }

        try {
            mFile.createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            mUploadFile.createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
        }

        Log.i(TAG, "createSaveFile previewFile=" + mFile.getPath() + ", uploadPath=" + mUploadFile.getPath());
    }

    private void onResumeProcess() {
        startBackgroundThread();
        Log.i(TAG, "onResumeProcess mTextureView.isAvailable()=" + mTextureView.isAvailable());
        // When the screen is turned off and turned back on, the SurfaceTexture is already
        // available, and "onSurfaceTextureAvailable" will not be called. In that case, we can open
        // a camera and start preview from here (otherwise, we wait until the surface is ready in
        // the SurfaceTextureListener).
        if (mTextureView.isAvailable()) {
            openCamera(mTextureView.getWidth(), mTextureView.getHeight(), mFacing);
        } else {
            mTextureView.setSurfaceTextureListener(mSurfaceTextureListener);
        }

        setOrientationSensor();
    }

    @Override
    public void onResume() {
        super.onResume();
        onResumeProcess();
    }

    private void requestCameraPermission() {
        ensurePermissions(new String[]{Manifest.permission.CAMERA});
//        if (shouldShowRequestPermissionRationale(Manifest.permission.CAMERA)) {
////            new ConfirmationDialog().show(getChildFragmentManager(), FRAGMENT_DIALOG);
//            showDialog(getString(R.string.request_permission), new DialogInterface.OnClickListener() {
//                @Override
//                public void onClick(DialogInterface dialog, int which) {
//                    finish();
//                }
//            });
//        } else {
////            requestPermissions(new String[]{Manifest.permission.CAMERA}, REQUEST_CAMERA_PERMISSION);
//            ensurePermissions(new String[]{Manifest.permission.CAMERA});
//        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        if (requestCode == REQUEST_CAMERA_PERMISSION) {
            if (grantResults.length != 1 || grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                showDialog(getString(R.string.request_permission), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }
                });
            }
        } else {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    /**
     * Sets up member variables related to camera.
     *
     * @param width  The width of available size for camera preview
     * @param height The height of available size for camera preview
     */
    @SuppressWarnings("SuspiciousNameCombination")
    private void setUpCameraOutputs(int width, int height) {
        CameraManager manager = (CameraManager) getSystemService(Context.CAMERA_SERVICE);
        try {

            for (String cameraId : manager.getCameraIdList()) {
                CameraCharacteristics characteristics = manager.getCameraCharacteristics(cameraId);

                Log.i(TAG, "setUpCameraOutputs.cameraId=" + cameraId);
                // We don't use a front facing camera in this sample.
                Integer facing = characteristics.get(CameraCharacteristics.LENS_FACING);
//                if (facing != null && facing == CameraCharacteristics.LENS_FACING_BACK) {
                if (facing != null && facing == mFacing) {
                    continue;
                }

                StreamConfigurationMap map = characteristics.get(CameraCharacteristics.SCALER_STREAM_CONFIGURATION_MAP);
                if (map == null) {
                    continue;
                }

                if (BuildConfig.DEBUG)
                    for (Size size : map.getOutputSizes(ImageFormat.JPEG)) {
                        int outputWidth = size.getWidth();
                        int outputHeight = size.getHeight();

//                        Log.i(TAG, "getOutputSizes.width="+outputWidth +", height="+ outputHeight);   // 카메라 해상도 종류
                    }

                // For still image captures, we use the largest available size.
//                Size largest = Collections.max(Arrays.asList(map.getOutputSizes(ImageFormat.JPEG)), new CompareSizesByArea());
                Size largest = chooseVideoSize(map.getOutputSizes(ImageFormat.JPEG));
                mImageReader = ImageReader.newInstance(largest.getWidth(), largest.getHeight(), ImageFormat.JPEG, /*maxImages*/2);

                mImageReader.setOnImageAvailableListener(mOnImageAvailableListener, mBackgroundHandler);

                // Find out if we need to swap dimension to get the preview size relative to sensor
                // coordinate.
                int displayRotation = getWindowManager().getDefaultDisplay().getRotation();
                //noinspection ConstantConditions
                mSensorOrientation = characteristics.get(CameraCharacteristics.SENSOR_ORIENTATION);
                boolean swappedDimensions = false;
                switch (displayRotation) {
                    case Surface.ROTATION_0:
                    case Surface.ROTATION_180:
                        if (mSensorOrientation == 90 || mSensorOrientation == 270) {
                            swappedDimensions = true;
                        }
                        break;
                    case Surface.ROTATION_90:
                    case Surface.ROTATION_270:
                        if (mSensorOrientation == 0 || mSensorOrientation == 180) {
                            swappedDimensions = true;
                        }
                        break;
                    default:
                        Log.e(TAG, "Display rotation is invalid: " + displayRotation);
                }
                Log.i(TAG, "swappedDimensions=" + swappedDimensions);
//                Log.e(TAG, "Display displayRotation=" + displayRotation);

                Point displaySize = new Point();
                getWindowManager().getDefaultDisplay().getSize(displaySize);
                int rotatedPreviewWidth = width;
                int rotatedPreviewHeight = height;
                int maxPreviewWidth = displaySize.x;
                int maxPreviewHeight = displaySize.y;

                if (swappedDimensions) {
                    rotatedPreviewWidth = height;
                    rotatedPreviewHeight = width;
                    maxPreviewWidth = displaySize.y;
                    maxPreviewHeight = displaySize.x;
                }

                if (maxPreviewWidth > MAX_PREVIEW_WIDTH) {
                    maxPreviewWidth = MAX_PREVIEW_WIDTH;
                }

                if (maxPreviewHeight > MAX_PREVIEW_HEIGHT) {
                    maxPreviewHeight = MAX_PREVIEW_HEIGHT;
                }

                // Danger, W.R.! Attempting to use too large a preview size could  exceed the camera
                // bus' bandwidth limitation, resulting in gorgeous previews but the storage of
                // garbage capture data.
                mPreviewSize = chooseOptimalSize(map.getOutputSizes(SurfaceTexture.class),
                        rotatedPreviewWidth, rotatedPreviewHeight, maxPreviewWidth,
                        maxPreviewHeight, largest);

                // We fit the aspect ratio of TextureView to the size of preview we picked.
                int orientation = getResources().getConfiguration().orientation;
                Log.i(TAG, "orientation=" + orientation + ", getWidth=" + mPreviewSize.getWidth() + ", getHeight=" + mPreviewSize.getHeight());
                if (orientation == Configuration.ORIENTATION_LANDSCAPE) {
                    mTextureView.setAspectRatio(
                            mPreviewSize.getWidth(), mPreviewSize.getHeight());
                } else {
                    mTextureView.setAspectRatio(
                            mPreviewSize.getHeight(), mPreviewSize.getWidth());
                }

                // Check if the flash is supported.
                Boolean available = characteristics.get(CameraCharacteristics.FLASH_INFO_AVAILABLE);
                mFlashSupported = available == null ? false : available;
                mCameraId = cameraId;
                return;
            }
        } catch (CameraAccessException e) {
            e.printStackTrace();
        } catch (NullPointerException e) {
            showDialog(getString(R.string.camera_error), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    finish();
                }
            });
        }
    }

    /**
     * Opens the camera specified by {@link NewCamera2Activity#mCameraId}.
     */
    private void openCamera(int width, int height, int facing) {
        if (ContextCompat.checkSelfPermission(NewCamera2Activity.this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            requestCameraPermission();
            return;
        }
        setUpCameraOutputs(width, height);
        configureTransform(width, height);
        CameraManager manager = (CameraManager) getSystemService(Context.CAMERA_SERVICE);
        try {
            if (!mCameraOpenCloseLock.tryAcquire(2500, TimeUnit.MILLISECONDS)) {
                throw new RuntimeException("Time out waiting to lock camera opening.");
            }
            manager.openCamera(mCameraId, mStateCallback, mBackgroundHandler);
        } catch (CameraAccessException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            throw new RuntimeException("Interrupted while trying to lock camera opening.", e);
        }

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                int guideHeight = (int) (mTextureView.getWidth() * 0.7);    // 4:3 비율 height
                mGuideLayout.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, guideHeight));

                // 가이드라인 표시
                int textureViewWidth = mTextureView.getWidth();
                int textureViewHeight = mTextureView.getHeight();
                int guideLayoutHeight = mGuideLayout.getHeight();
                Log.i(TAG, "textureViewWidth=" + textureViewWidth + ", textureViewHeight.getHeight()=" + textureViewHeight + ", guideLayoutHeight=" + guideLayoutHeight + ", guideHeight=" + guideHeight);
            }
        });
    }

    /**
     * Closes the current {@link CameraDevice}.
     */
    private void closeCamera() {
        try {
            mCameraOpenCloseLock.acquire();
            if (null != mCaptureSession) {
                mCaptureSession.close();
                mCaptureSession = null;
            }
            if (null != mCameraDevice) {
                mCameraDevice.close();
                mCameraDevice = null;
            }
            if (null != mImageReader) {
                mImageReader.close();
                mImageReader = null;
            }
        } catch (InterruptedException e) {
            throw new RuntimeException("Interrupted while trying to lock camera closing.", e);
        } finally {
            mCameraOpenCloseLock.release();
        }
    }

    /**
     * Starts a background thread and its {@link Handler}.
     */
    private void startBackgroundThread() {
        mBackgroundThread = new HandlerThread("CameraBackground");
        mBackgroundThread.start();
        mBackgroundHandler = new Handler(mBackgroundThread.getLooper());
    }

    /**
     * Stops the background thread and its {@link Handler}.
     */
    private void stopBackgroundThread() {
        if (mBackgroundThread == null)
            return;

        mBackgroundThread.quitSafely();
        try {
            mBackgroundThread.join();
            mBackgroundThread = null;
            mBackgroundHandler = null;
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    /**
     * Creates a new {@link CameraCaptureSession} for camera preview.
     */
    private void createCameraPreviewSession() {
        try {
            SurfaceTexture texture = mTextureView.getSurfaceTexture();
            assert texture != null;

            // We configure the size of default buffer to be the size of camera preview we want.
            texture.setDefaultBufferSize(mPreviewSize.getWidth(), mPreviewSize.getHeight());

            // This is the output Surface we need to start preview.
            Surface surface = new Surface(texture);

            // We set up a CaptureRequest.Builder with the output Surface.
            mPreviewRequestBuilder = mCameraDevice.createCaptureRequest(CameraDevice.TEMPLATE_PREVIEW);
            mPreviewRequestBuilder.addTarget(surface);

            // Here, we create a CameraCaptureSession for camera preview.
            mCameraDevice.createCaptureSession(Arrays.asList(surface, mImageReader.getSurface()),
                    new CameraCaptureSession.StateCallback() {

                        @Override
                        public void onConfigured(@NonNull CameraCaptureSession cameraCaptureSession) {
                            // The camera is already closed
                            if (null == mCameraDevice) {
                                return;
                            }

                            // When the session is ready, we start displaying the preview.
                            mCaptureSession = cameraCaptureSession;
                            try {
                                // Auto focus should be continuous for camera preview.
                                mPreviewRequestBuilder.set(CaptureRequest.CONTROL_AF_MODE, CaptureRequest.CONTROL_AF_MODE_CONTINUOUS_PICTURE);
                                // Flash is automatically enabled when necessary.
                                setAutoFlash(mPreviewRequestBuilder);

                                // Finally, we start displaying the camera preview.
                                mPreviewRequest = mPreviewRequestBuilder.build();
                                mCaptureSession.setRepeatingRequest(mPreviewRequest, mCaptureCallback, mBackgroundHandler);

                                mShutterBtn.setEnabled(true);
                            } catch (CameraAccessException e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onConfigureFailed(
                                @NonNull CameraCaptureSession cameraCaptureSession) {
                            showToast("Preview Failed");
                        }
                    }, null
            );
        } catch (CameraAccessException e) {
            e.printStackTrace();
        }
    }

    /**
     * Configures the necessary {@link Matrix} transformation to `mTextureView`.
     * This method should be called after the camera preview size is determined in
     * setUpCameraOutputs and also the size of `mTextureView` is fixed.
     *
     * @param viewWidth  The width of `mTextureView`
     * @param viewHeight The height of `mTextureView`
     */
    private int mConfigurationRotation = -999;

    private void configureTransform(int viewWidth, int viewHeight) {
        if (null == mTextureView || null == mPreviewSize) {
            return;
        }
        int rotation = getWindowManager().getDefaultDisplay().getRotation();
        Matrix matrix = new Matrix();
        RectF viewRect = new RectF(0, 0, viewWidth, viewHeight);
        RectF bufferRect = new RectF(0, 0, mPreviewSize.getHeight(), mPreviewSize.getWidth());
        float centerX = viewRect.centerX();
        float centerY = viewRect.centerY();
        if (Surface.ROTATION_90 == rotation || Surface.ROTATION_270 == rotation) {
            bufferRect.offset(centerX - bufferRect.centerX(), centerY - bufferRect.centerY());
            matrix.setRectToRect(viewRect, bufferRect, Matrix.ScaleToFit.FILL);
            float scale = Math.max(
                    (float) viewHeight / mPreviewSize.getHeight(),
                    (float) viewWidth / mPreviewSize.getWidth());
            matrix.postScale(scale, scale, centerX, centerY);
            matrix.postRotate(90 * (rotation - 2), centerX, centerY);
        } else if (Surface.ROTATION_180 == rotation) {
            matrix.postRotate(180, centerX, centerY);
        }
        Log.i(TAG, "configureTransform.rotation=" + rotation);
        mConfigurationRotation = rotation;
        mTextureView.setTransform(matrix);
    }

    /**
     * Initiate a still image capture.
     */
    private void takePicture() {
        createSaveFile();
        lockFocus();
    }

    /**
     * Lock the focus as the first step for a still image capture.
     */
    private void lockFocus() {
        if (mCaptureSession == null) {
            Log.e(TAG, "mCaptureSession is null");
            return;
        }
        try {
            // This is how to tell the camera to lock focus.
            mPreviewRequestBuilder.set(CaptureRequest.CONTROL_AF_TRIGGER, CameraMetadata.CONTROL_AF_TRIGGER_START);
            // Tell #mCaptureCallback to wait for the lock.
            mState = STATE_WAITING_LOCK;
            mCaptureSession.capture(mPreviewRequestBuilder.build(), mCaptureCallback, mBackgroundHandler);
        } catch (CameraAccessException e) {
            e.printStackTrace();
        } finally {
        }

    }

    /**
     * Run the precapture sequence for capturing a still image. This method should be called when
     * we get a response in {@link #mCaptureCallback} from {@link #lockFocus()}.
     */
    private void runPrecaptureSequence() {
        try {
            // This is how to tell the camera to trigger.
            mPreviewRequestBuilder.set(CaptureRequest.CONTROL_AE_PRECAPTURE_TRIGGER, CaptureRequest.CONTROL_AE_PRECAPTURE_TRIGGER_START);
            // Tell #mCaptureCallback to wait for the precapture sequence to be set.
            mState = STATE_WAITING_PRECAPTURE;
            mCaptureSession.capture(mPreviewRequestBuilder.build(), mCaptureCallback, mBackgroundHandler);
        } catch (CameraAccessException e) {
            e.printStackTrace();
        }
    }

    /**
     * Capture a still picture. This method should be called when we get a response in
     * {@link #mCaptureCallback} from both {@link #lockFocus()}.
     */
    private void captureStillPicture() {

//        soundEffect();

        try {
            Log.i(TAG, "captureStillPicture 2 mCameraDevice=" + mCameraDevice);
            if (null == mCameraDevice) {
                return;
            }
            // This is the CaptureRequest.Builder that we use to take a picture.
            final CaptureRequest.Builder captureBuilder = mCameraDevice.createCaptureRequest(CameraDevice.TEMPLATE_STILL_CAPTURE);

//            Surface surface = mImageReader.getSurface();
            captureBuilder.addTarget(mImageReader.getSurface());

            // Use the same AE and AF modes as the preview.
            captureBuilder.set(CaptureRequest.CONTROL_AF_MODE, CaptureRequest.CONTROL_AF_MODE_CONTINUOUS_PICTURE);
            setAutoFlash(captureBuilder);

            // Orientation  회전 오류 발생 됨
//            int rotation = activity.getWindowManager().getDefaultDisplay().getRotation();
//            captureBuilder.set(CaptureRequest.JPEG_ORIENTATION, getOrientation(rotation));

            CameraCaptureSession.CaptureCallback callback = new CameraCaptureSession.CaptureCallback() {

                @Override
                public void onCaptureCompleted(@NonNull CameraCaptureSession session,
                                               @NonNull final CaptureRequest request,
                                               @NonNull TotalCaptureResult result) {
//                    showToast("Saved: " + mFile);
                    Log.d(TAG, "Saved: " + mFile.toString());
                    unlockFocus();
                }
            };

            mCaptureSession.stopRepeating();
            mCaptureSession.abortCaptures();
            mCaptureSession.capture(captureBuilder.build(), callback, null);
        } catch (CameraAccessException e) {
            e.printStackTrace();
        } finally {
            if (mFile != null) {
                MediaScannerConnection.scanFile(NewCamera2Activity.this,
                        new String[]{mFile.getPath()}, null,
                        new MediaScannerConnection.OnScanCompletedListener() {
                            public void onScanCompleted(String path, Uri uri) {
                                Log.i(TAG, "ExternalStorage Scanned " + path + "-> uri=" + uri);
                                NewResultActivity.startResultActivity(NewCamera2Activity.this, mFile.getPath(), mUploadFile.getPath());
                            }
                        });

            }
        }
    }

    /**
     * Retrieves the JPEG orientation from the specified screen rotation.
     *
     * @param rotation The screen rotation.
     * @return The JPEG orientation (one of 0, 90, 270, and 360)
     */
    private int getOrientation(int rotation) {
        // Sensor orientation is 90 for most devices, or 270 for some devices (eg. Nexus 5X)
        // We have to take that into account and rotate JPEG properly.
        // For devices with orientation of 90, we simply return our mapping from ORIENTATIONS.
        // For devices with orientation of 270, we need to rotate the JPEG 180 degrees.
        return (ORIENTATIONS.get(rotation) + mSensorOrientation + 270) % 360;
    }

    /**
     * Unlock the focus. This method should be called when still image capture sequence is
     * finished.
     */
    private void unlockFocus() {
        try {
            // Reset the auto-focus trigger
            mPreviewRequestBuilder.set(CaptureRequest.CONTROL_AF_TRIGGER, CameraMetadata.CONTROL_AF_TRIGGER_CANCEL);
            setAutoFlash(mPreviewRequestBuilder);

            if (mCaptureSession == null) {
                Log.e(TAG, "mCaptureSession is null... createCameraPreviewSession()");
                createCameraPreviewSession();
                mState = STATE_PREVIEW;
            } else {
                mCaptureSession.capture(mPreviewRequestBuilder.build(), mCaptureCallback, mBackgroundHandler);
                // After this, the camera will go back to the normal state of preview.
                mState = STATE_PREVIEW;
                mCaptureSession.setRepeatingRequest(mPreviewRequest, mCaptureCallback, mBackgroundHandler);
            }

        } catch (CameraAccessException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.shutter_btn: {
                view.setEnabled(false);
                takePicture();
                break;
            }
            case R.id.facing_btn: {
                // 카메라 전후면 전환

                closeCamera();
                mFacing = (mFacing == CameraCharacteristics.LENS_FACING_BACK) ? CameraCharacteristics.LENS_FACING_FRONT : CameraCharacteristics.LENS_FACING_BACK;
//                    setOrientationActivity();
                SharedPref.getInstance(NewCamera2Activity.this).savePreferences(SharedPref.CAMERA_FACING, mFacing);
                openCamera(mTextureView.getWidth(), mTextureView.getHeight(), mFacing);
                break;
            }
            case R.id.camera_finish_btn: {
                finish();
                break;
            }
        }
    }

    private void setAutoFlash(CaptureRequest.Builder requestBuilder) {
        if (mFlashSupported) {
            requestBuilder.set(CaptureRequest.CONTROL_AE_MODE,
                    CaptureRequest.CONTROL_AE_MODE_ON_AUTO_FLASH);
        }
    }

    /**
     * Saves a JPEG {@link Image} into the specified {@link File}.
     */
    private class ImageSaver implements Runnable {
        private final Image mImage;
        private final File mFile;

        ImageSaver(Image image, File file) {
            mImage = image;
            mFile = file;
        }

        /**
         * 이미지 잘라내기
         *
         * @param original
         * @return
         */
        public Bitmap cropBitmap(Bitmap original) {
            int bitmapHeight = original.getHeight();
            int guideHeight = (int) (original.getWidth() * 0.7);
            int yPos = (bitmapHeight / 2) - (guideHeight / 2);
            int yEnd = guideHeight;

            Log.i(TAG, "bitmapHeight=" + bitmapHeight + ", yPos=" + yPos + ", yEnd=" + yEnd);
            Bitmap result = Bitmap.createBitmap(original
                    , 0 //X 시작위치
                    , yPos //Y 시작위치
                    , original.getWidth() // x의 종료 위치
                    , yEnd); // y의 종료 위치

            if (result != original) {
                original.recycle();
            }
            return result;
        }

        private Bitmap convertImageToBitmap(Image image) throws OutOfMemoryError {
            ByteBuffer buffer = image.getPlanes()[0].getBuffer();
            byte[] bytes = new byte[buffer.capacity()];
            buffer.get(bytes);
            Bitmap bitmap = BitmapFactory.decodeByteArray(bytes, 0, bytes.length, null);

            Matrix rotateMatrix = new Matrix();
            Log.i(TAG, "rotation=" + mOrientation);
            rotateMatrix.postRotate(exifOrientationToDegrees(mOrientation));  // 회전 오류 수정
//            rotateMatrix.postRotate(mOrientation);

            if (bitmap == null)
                return bitmap;

            bitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), rotateMatrix, false);

            // 전면카메라일경우 좌우 반전 처리
            if (mFacing == CameraCharacteristics.LENS_FACING_BACK) {
                bitmap = reverseBitmap(bitmap);
            } else {

            }
            return bitmap;
        }

        /**
         * 이미지 좌우 반전 처리
         *
         * @param bitmap
         * @return
         */
        private Bitmap reverseBitmap(Bitmap bitmap) {
            Matrix sideInversion = new Matrix();
//                sideInversion.setScale(1, -1);  // 상하반전
            sideInversion.setScale(-1, 1);  // 좌우반전
            bitmap = Bitmap.createBitmap(bitmap, 0, 0,
                    bitmap.getWidth(), bitmap.getHeight(), sideInversion, false);

            return bitmap;
        }

        /**
         * 이미지 회전 오류 처리
         * LENS_FACING_FRONT > 후면 카메라
         *
         * @param exifOrientation
         * @return
         */
        public int exifOrientationToDegrees(int exifOrientation) {
            if (exifOrientation == ExifInterface.ORIENTATION_ROTATE_90) {
                Log.i(TAG, "exifOrientationToDegrees_90");
                return mFacing == CameraCharacteristics.LENS_FACING_FRONT ? 180 : 180;
            } else if (exifOrientation == ExifInterface.ORIENTATION_ROTATE_180) {
                Log.i(TAG, "exifOrientationToDegrees_180");
                return mFacing == CameraCharacteristics.LENS_FACING_FRONT ? 270 : 90;
            } else if (exifOrientation == ExifInterface.ORIENTATION_ROTATE_270) {
                Log.i(TAG, "exifOrientationToDegrees_270");
                return mFacing == CameraCharacteristics.LENS_FACING_FRONT ? 0 : 0;
            } else if (exifOrientation == ExifInterface.ORIENTATION_NORMAL) {
                Log.i(TAG, "exifOrientationToDegrees_Normal");
                return mFacing == CameraCharacteristics.LENS_FACING_FRONT ? 90 : 270;
            }
            return 0;
        }

        @Override
        public void run() {
            Log.i(TAG, mFile.getPath());
            FileOutputStream out = null;
            FileOutputStream out2 = null;
            try {
                out = new FileOutputStream(mFile);
                out2 = new FileOutputStream(mUploadFile);
                Bitmap bitmap = convertImageToBitmap(mImage);

                if (bitmap == null)
                    return;

                soundEffect();

                bitmap = cropBitmap(bitmap);    // 4:3 으로 이미지 자르기
                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, out);

                if (mFacing == CameraCharacteristics.LENS_FACING_FRONT) {
                    bitmap = reverseBitmap(bitmap); // 업로드용 이미지 좌우 반전 처리
                    bitmap.compress(Bitmap.CompressFormat.JPEG, 100, out2);
                }
            } catch (FileNotFoundException e) {
                e.printStackTrace();
//                } catch (IOException e) {
//                    e.printStackTrace();
            } finally {
                mImage.close();
                if (null != out) {
                    try {
                        out.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }

                if (null != out2) {
                    try {
                        out2.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

        }
    }

    /**
     * Compares two {@code Size}s based on their areas.
     */
    static class CompareSizesByArea implements Comparator<Size> {

        @Override
        public int compare(Size lhs, Size rhs) {
            // We cast here to ensure the multiplications won't overflow
            return Long.signum((long) lhs.getWidth() * lhs.getHeight() -
                    (long) rhs.getWidth() * rhs.getHeight());
        }
    }

    public void showDialog(String message, DialogInterface.OnClickListener clickListener) {
        new AlertDialog.Builder(this)
                .setMessage(message)
                .setPositiveButton(android.R.string.ok, clickListener)
                .create()
                .show();
    }


//    /**
//     * Shows an error message dialog.
//     */
//    public static class ErrorDialog extends DialogFragment {
//
//        private static final String ARG_MESSAGE = "message";
//
//        public static ErrorDialog newInstance(String message) {
//            ErrorDialog dialog = new ErrorDialog();
//            Bundle args = new Bundle();
//            args.putString(ARG_MESSAGE, message);
//            dialog.setArguments(args);
//            return dialog;
//        }
//
//        @NonNull
//        @Override
//        public Dialog onCreateDialog(Bundle savedInstanceState) {
//            final Activity activity = getActivity();
//            return new AlertDialog.Builder(activity)
//                    .setMessage(getArguments().getString(ARG_MESSAGE))
//                    .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
//                        @Override
//                        public void onClick(DialogInterface dialogInterface, int i) {
//                            activity.finish();
//                        }
//                    })
//                    .create();
//        }
//    }
//
//    /**
//     * Shows OK/Cancel confirmation dialog about camera permission.
//     */
//    public static class ConfirmationDialog extends DialogFragment {
//
//        @NonNull
//        @Override
//        public Dialog onCreateDialog(Bundle savedInstanceState) {
//            final Fragment parent = getParentFragment();
//            return new AlertDialog.Builder(getActivity())
//                    .setMessage(R.string.request_permission)
//                    .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
//                        @Override
//                        public void onClick(DialogInterface dialog, int which) {
//                            parent.requestPermissions(new String[]{Manifest.permission.CAMERA},
//                                    REQUEST_CAMERA_PERMISSION);
//                        }
//                    })
//                    .setNegativeButton(android.R.string.cancel,
//                            new DialogInterface.OnClickListener() {
//                                @Override
//                                public void onClick(DialogInterface dialog, int which) {
//                                    Activity activity = parent.getActivity();
//                                    if (activity != null) {
//                                        activity.finish();
//                                    }
//                                }
//                            })
//                    .create();
//        }
//    }


    public float finger_spacing = 0;
    public int zoom_level = 1;

    public boolean onZoomTouch(View v, MotionEvent event) {
        try {
            CameraManager manager = (CameraManager) getSystemService(Context.CAMERA_SERVICE);
            CameraCharacteristics characteristics = manager.getCameraCharacteristics(mCameraId);
            float maxzoom = (characteristics.get(CameraCharacteristics.SCALER_AVAILABLE_MAX_DIGITAL_ZOOM)) * 10;

            Log.i(TAG, "maxzoom=" + maxzoom);

            Rect m = characteristics.get(CameraCharacteristics.SENSOR_INFO_ACTIVE_ARRAY_SIZE);
            int action = event.getAction();
            float current_finger_spacing;

            if (event.getPointerCount() > 1) {
                // Multi touch logic
                current_finger_spacing = getFingerSpacing(event);
                if (finger_spacing != 0) {
                    if (current_finger_spacing > finger_spacing && maxzoom > zoom_level) {
                        zoom_level++;
                    } else if (current_finger_spacing < finger_spacing && zoom_level > 1) {
                        zoom_level--;
                    }
                    int minW = (int) (m.width() / maxzoom);
                    int minH = (int) (m.height() / maxzoom);
                    int difW = m.width() - minW;
                    int difH = m.height() - minH;
                    int cropW = difW / 100 * (int) zoom_level;
                    int cropH = difH / 100 * (int) zoom_level;
                    cropW -= cropW & 3;
                    cropH -= cropH & 3;
                    Rect zoom = new Rect(cropW, cropH, m.width() - cropW, m.height() - cropH);
                    mPreviewRequestBuilder.set(CaptureRequest.SCALER_CROP_REGION, zoom);
                }
                finger_spacing = current_finger_spacing;
            } else {
                if (action == MotionEvent.ACTION_UP) {
                    //single touch logic
                }
            }

            try {
                mCaptureSession.setRepeatingRequest(mPreviewRequestBuilder.build(), mCaptureCallback, null);
            } catch (CameraAccessException e) {
                e.printStackTrace();
            } catch (NullPointerException ex) {
                ex.printStackTrace();
            }
        } catch (CameraAccessException e) {
            throw new RuntimeException("can not access camera.", e);
        }
        return true;
    }

    public boolean zoomControl(int keyCode) {
        try {
            CameraManager manager = (CameraManager) getSystemService(Context.CAMERA_SERVICE);
            CameraCharacteristics characteristics = manager.getCameraCharacteristics(mCameraId);
            float maxzoom = (characteristics.get(CameraCharacteristics.SCALER_AVAILABLE_MAX_DIGITAL_ZOOM)) * 10;

            Log.i(TAG, "maxzoom=" + maxzoom + ", zoom_level=" + zoom_level);

            Rect m = characteristics.get(CameraCharacteristics.SENSOR_INFO_ACTIVE_ARRAY_SIZE);

            if (keyCode == KeyEvent.KEYCODE_VOLUME_UP) {
                zoom_level++;
            } else if (keyCode == KeyEvent.KEYCODE_VOLUME_DOWN) {
                zoom_level--;
            }

            int minW = (int) (m.width() / maxzoom);
            int minH = (int) (m.height() / maxzoom);
            int difW = m.width() - minW;
            int difH = m.height() - minH;
            int cropW = difW / 100 * (int) zoom_level;
            int cropH = difH / 100 * (int) zoom_level;
            cropW -= cropW & 3;
            cropH -= cropH & 3;
            Rect zoom = new Rect(cropW, cropH, m.width() - cropW, m.height() - cropH);
            mPreviewRequestBuilder.set(CaptureRequest.SCALER_CROP_REGION, zoom);

            try {
                mCaptureSession.setRepeatingRequest(mPreviewRequestBuilder.build(), mCaptureCallback, null);
            } catch (CameraAccessException e) {
                e.printStackTrace();
            } catch (NullPointerException ex) {
                ex.printStackTrace();
            }
        } catch (CameraAccessException e) {
            throw new RuntimeException("can not access camera.", e);
        }
        return false;
    }

    /**
     * 셔터음
     */
    private MediaActionSound mSound;
//    Ringtone mRing;

    private void soundEffect() {
//        if (BuildConfig.DEBUG)
//            return;

//        soundPool.play(sm[0], 1, 1, 1, 0, 1f);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    mSound = new MediaActionSound();
                    mSound.load(MediaActionSound.SHUTTER_CLICK);
                    mSound.play(MediaActionSound.SHUTTER_CLICK);
                }
            });

    }
    // 강제로 소리
//        Resources resources = NewCamera2Activity.this.getResources();
//        Uri shutterUri = Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE + "://" + resources.getResourcePackageName(R.raw.shutter) + '/' + resources.getResourceTypeName(R.raw.shutter) + '/' + resources.getResourceEntryName(R.raw.shutter) );
//
//        final AudioManager manager = (AudioManager)getActivity().getSystemService(Activity.AUDIO_SERVICE);
//        int max = manager.getStreamMaxVolume(AudioManager.RINGER_MODE_NORMAL);
//        manager.setRingerMode(AudioManager.RINGER_MODE_NORMAL); // 벨소리 모드로 변경
//        manager.setStreamVolume(AudioManager.STREAM_RING, max, AudioManager.FLAG_PLAY_SOUND);
//
////        mRing = RingtoneManager.getRingtone(NewCamera2Activity.this, RingtoneManager.getActualDefaultRingtoneUri(NewCamera2Activity.this,RingtoneManager.TYPE_RINGTONE));
//        Ringtone mRing = RingtoneManager.getRingtone(NewCamera2Activity.this, shutterUri);
//        mRing.play();

        // 강제로 셔터소리 2

//        MediaPlayer mMediaPlayer = new MediaPlayer();
//        try {
//            mMediaPlayer.setDataSource(NewCamera2Activity.this, shutterUri);
//
//            final AudioManager audioManager = (AudioManager)NewCamera2Activity.this.getSystemService(Context.AUDIO_SERVICE);
//
//            if (audioManager.getStreamVolume(AudioManager.STREAM_ALARM) != 0) {
//                player.setAudioStreamType(AudioManager.STREAM_ALARM);
//                player.setLooping(true);
//                player.prepare();
//                player.start();
//            }
//        } catch (IOException e) {
//            e.printStackTrace();
//        }

    }


    //Determine the space between the first two fingers
    @SuppressWarnings("deprecation")
    private float getFingerSpacing(MotionEvent event) {
        float x = event.getX(0) - event.getX(1);
        float y = event.getY(0) - event.getY(1);
        return (float) Math.sqrt(x * x + y * y);
    }

//    public boolean onKeyDown(int keyCode, KeyEvent event) {
//        if (keyCode == KeyEvent.KEYCODE_VOLUME_DOWN) {
//            zoomControl(+1);
//        } else if (keyCode == KeyEvent.KEYCODE_VOLUME_UP) {
//            zoomControl(-1);
//        }
//        return false;
//    }

    @Override
    public void onPause() {
        super.onPause();
        onPauseProcess();
        if (mSound != null)
            mSound.release();

    }

    private void onPauseProcess() {
        if (orientEventListener != null)
            orientEventListener.disable();
        closeCamera();
        stopBackgroundThread();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        Log.i(TAG, "onConfigurationChanged.orientation=" + newConfig.orientation);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
//        IntentResult result = IntentIntegrator.parseActivityResult(resultCode, data);
        Log.i(TAG, "onActivityResult.requestCode="+requestCode+", resultCode="+resultCode+", data="+data);
        if (requestCode == MainActivity.REQUEST_CODE_TOOTH_CAMERA) {
            Log.i(TAG, "onActivityResult.requestCode="+requestCode+", resultCode="+resultCode+", data="+data);
            if (resultCode == Activity.RESULT_OK) {

                setResult(Activity.RESULT_OK, data);
                finish();
            }
        }
    }
}
