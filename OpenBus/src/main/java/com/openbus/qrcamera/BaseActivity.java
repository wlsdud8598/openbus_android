package com.openbus.qrcamera;

import android.Manifest;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.LinearLayout;

import java.util.ArrayList;

import androidx.annotation.DrawableRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

public class BaseActivity extends AppCompatActivity {
    protected final String TAG = getClass().getSimpleName();
    private LinearLayout mProgressLayout;
    private LinearLayout mProgressLayout_txt;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void setContentView(int layoutResID) {
        super.setContentView(R.layout.activity_base);
        LinearLayout baseLayout = findViewById(R.id.base_content_layout);
        mProgressLayout = findViewById(R.id.base_progressBar);
        mProgressLayout_txt = findViewById(R.id.base_progressBar_text);

        View view = LayoutInflater.from(this).inflate(layoutResID, null);
        view.setLayoutParams(new FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT, FrameLayout.LayoutParams.MATCH_PARENT));

        baseLayout.addView(view);
    }

    /**
     * Permission Setting
     * @param permissions
     */
    protected void ensurePermissions(String... permissions) {
        ArrayList<String> deniedPermissionList = new ArrayList<>();

        for (String permission : permissions) {
            if (PackageManager.PERMISSION_GRANTED != ContextCompat.checkSelfPermission(this, permission))
                deniedPermissionList.add(permission);
        }

        if (!deniedPermissionList.isEmpty()) {
            ActivityCompat.requestPermissions(this, deniedPermissionList.toArray(new String[0]), 0);
        } else {
            onPermissionResult();
        }
    }

    protected void onPermissionResult() {

    }


    /**
     * Shows alert dialog.
     */
    protected void showAlertDialog(final String message, final boolean finishActivity) {
        AlertDialogFragment.newInstance(android.R.drawable.ic_dialog_alert,
                "알림",
                message,
                finishActivity).show(getFragmentManager(), "alert_dialog");
    }

    /**
     * Alert Dialog Fragment
     */
    public static class AlertDialogFragment extends DialogFragment {
        public static AlertDialogFragment newInstance(@DrawableRes int iconId, CharSequence title, CharSequence message, boolean finishActivity) {
            AlertDialogFragment fragment = new AlertDialogFragment();

            Bundle args = new Bundle();
            args.putInt("icon", iconId);
            args.putCharSequence("title", title);
            args.putCharSequence("message", message);
            args.putBoolean("finish_activity", finishActivity);

            fragment.setArguments(args);
            return fragment;
        }

        @NonNull
        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            boolean finishActivity = getArguments().getBoolean("finish_activity");

            return new AlertDialog.Builder(getActivity())
                    .setTitle(getArguments().getCharSequence("title"))
                    .setIcon(getArguments().getInt("icon"))
                    .setMessage(getArguments().getCharSequence("message"))
                    .setPositiveButton(android.R.string.ok, finishActivity ?
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                    getActivity().finish();
                                }
                            } : null)
                    .setCancelable(false)
                    .create();
        }
    }

    public void showProgress() {
        mProgressLayout.setVisibility(View.VISIBLE);
        mProgressLayout_txt.setVisibility(View.GONE);
    }

    public void hideProgress() {
        mProgressLayout.setVisibility(View.GONE);
    }

    public void showProgress_txt() {
        mProgressLayout.setVisibility(View.GONE);
        mProgressLayout_txt.setVisibility(View.VISIBLE);
    }

    public void hideProgress_txt() {
        mProgressLayout_txt.setVisibility(View.GONE);
    }


//    @Override
//    public void finish() {
//        super.finish();
//        overridePendingTransitionExit();
//    }

    @Override
    public void startActivity(Intent intent) {
        super.startActivity(intent);
        overridePendingTransitionEnter();
    }

    /**
     * Overrides the pending Activity transition by performing the "Enter" animation.
     */
    protected void overridePendingTransitionEnter() {
        overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
    }

    /**
     * Overrides the pending Activity transition by performing the "Exit" animation.
     */
    protected void overridePendingTransitionExit() {
        overridePendingTransition(R.anim.slide_from_left, R.anim.slide_to_right);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        for (int result : grantResults) {
            if (result != PackageManager.PERMISSION_GRANTED) {
                showAlertDialog("Requested permission is not granted.", true);
                onPermissionResult();
                break;
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        ensurePermissions(Manifest.permission.CAMERA
                , Manifest.permission.READ_EXTERNAL_STORAGE
                , Manifest.permission.WRITE_EXTERNAL_STORAGE);
    }

}
