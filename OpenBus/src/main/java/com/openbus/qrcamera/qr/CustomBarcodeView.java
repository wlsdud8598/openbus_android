package com.openbus.qrcamera.qr;

import android.content.Context;
import android.util.AttributeSet;

import com.journeyapps.barcodescanner.DecoratedBarcodeView;

public class CustomBarcodeView  extends DecoratedBarcodeView {

    public CustomBarcodeView(Context context) {
        super(context);
    }

    public CustomBarcodeView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public CustomBarcodeView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }
}
